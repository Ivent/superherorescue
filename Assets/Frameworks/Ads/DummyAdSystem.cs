﻿
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Framework.Managers.Ads
{
    public class DummyAdSystem : BaseAdSystem
    {
        public override KeyValuePair<string, double> GetRewardParameters()
        {
            return new KeyValuePair<string, double>("coins",1);
        }

        public override void Init()
        {
            if (!AdIsEnable()) return;
            Debug.Log("DUMMYADS: Инициализация завершена");
        }
           
        public override void ShowBanner()
        {
            if (!AdIsEnable()) return;
            Debug.Log("DUMMYADS: Показать баннер");
        }

        public override void HideBanner()
        {
            if (!AdIsEnable()) return;
            Debug.Log("DUMMYADS: Скрыть баннер");
        }

        public override void ShowInterstetial(Action<bool> IsLoaded)
        {
            if (!AdIsEnable())
            {
                IsLoaded(false);
                return;
            }
            IsLoaded(true);
            Debug.Log("DUMMYADS: Показать межстраничку");
            if ( interstitialAdListener != null)
            {
                interstitialAdListener.OnInterstitialShown();
                interstitialAdListener.OnInterstitialClosed();
            }
         
           
        }

        public override void ShowRewardedVideo(Action<bool> IsLoaded)
        {
            IsLoaded(true);
            Debug.Log("DUMMYADS: Показать видео с наградой");
            if (rewardedVideoAdListener != null)
            {
                Debug.Log("DUMMYADS: Показ видео с наградой успешно завершен");
                rewardedVideoAdListener.OnRewardedVideoShown();
                rewardedVideoAdListener.OnRewardedVideoFinished(1, "coins");
            }

        }

        public override bool rewardVideoIsLoaded()
        {
            Debug.Log("DUMMYADS: Проверка на загрузку рекламы результат: true" );
            return true;
        }
    }
}
