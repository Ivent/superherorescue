﻿
//using System;
//using System.Collections.Generic;
//using Adic;
//using AppodealAds.Unity.Api;
//using AppodealAds.Unity.Common;
//using UnityEngine;

//namespace Framework.Managers.Ads
//{
//    public class AppodealAdSystem : BaseAdSystem,IInterstitialAdListener,IRewardedVideoAdListener
//    {

//        string appodealAndroidKey = "9c848c24a6ea865102ebf847b0bcf535f69bb3194287ebc9";
//        string appodealIosKey;

//        string appodealKey;


//        public override void Init()
//        {
            
//            if (!AdIsEnable()) return;

//            Debug.Log("APPODEAL: Инициализация началась");

//            string androidKey = appodealAndroidKey;
//            string iosKey = appodealIosKey;


//            #if UNITY_ANDROID 
//                        appodealKey = androidKey;
//            #elif UNITY_IOS
//                        appodealKey = iosKey;
//            #endif

//            Appodeal.initialize(appodealKey, Appodeal.BANNER | Appodeal.INTERSTITIAL | Appodeal.REWARDED_VIDEO);
//            Appodeal.setInterstitialCallbacks(this);
//            Appodeal.setRewardedVideoCallbacks(this);

//            Debug.Log("APPODEAL: Инициализация завершена  " + appodealKey);

//        }

//        public override KeyValuePair<string, double> GetRewardParameters()
//        {
//            return Appodeal.getRewardParameters("rewarded_video");
//        }

//        public override void ShowBanner()
//        {
//            if (!AdIsEnable()) return;
//            if (Appodeal.isLoaded(Appodeal.BANNER_BOTTOM))
//            {
//                Appodeal.show(Appodeal.BANNER_BOTTOM);
//            }

//            Debug.Log("APPODEAL: Показать баннер");
//        }

//        public override void HideBanner()
//        {
//            if (!AdIsEnable()) return;

//            Appodeal.hide(Appodeal.BANNER_BOTTOM);
//            Debug.Log("APPODEAL: Скрыть баннер");
//        }

//        public override void ShowInterstetial(Action<bool> IsLoaded)
//        {
//            if (!AdIsEnable())
//            {
//                IsLoaded(false);
//                return;
//            }

//            if (Appodeal.isLoaded(Appodeal.INTERSTITIAL))
//            {
//                IsLoaded(true);
//                Appodeal.show(Appodeal.INTERSTITIAL);

//            }
//            else
//            {
//                IsLoaded(false);
//            }
//            Debug.Log("APPODEAL: Показать межстраничку");
//        }

//        public override void ShowRewardedVideo(Action<bool> IsLoaded)
//        {
//            if (Appodeal.isLoaded(Appodeal.REWARDED_VIDEO))
//            {
//                IsLoaded(true);
//                Appodeal.show(Appodeal.REWARDED_VIDEO);
//            }
//            else
//            {
//                IsLoaded(false);
//            }
//            Debug.Log("APPODEAL: Показать видео с наградой");
//        }

//        public void onInterstitialShown()
//        {
//            if (interstitialAdListener != null)
//                interstitialAdListener.OnInterstitialShown();
//        }

//        public void onInterstitialClosed()
//        {
//            if (interstitialAdListener != null)
//                interstitialAdListener.OnInterstitialClosed();
//        }

//        public void onInterstitialClicked()
//        {
//            if (interstitialAdListener != null)
//                interstitialAdListener.OnInterstitialClicked();
//        }

//        public void onInterstitialLoaded(bool isPrecache){}
//        public void onInterstitialFailedToLoad(){
//            if (interstitialAdListener != null)
//                interstitialAdListener.OnInterstitialFailedToLoad();
//        }
//        public void onInterstitialExpired(){}

//        public void onRewardedVideoLoaded(bool precache)
//        {
//            if (rewardedVideoAdListener != null)
//                rewardedVideoAdListener.OnRewardedVideoLoaded();
//        }

//        public void onRewardedVideoFailedToLoad()
//        {
//            if (rewardedVideoAdListener != null)
//                rewardedVideoAdListener.OnRewardedVideoFailedToLoad();
//        }

//        public void onRewardedVideoShown()
//        {
//            if (rewardedVideoAdListener != null)
//                rewardedVideoAdListener.OnRewardedVideoShown();
//        }

//        public void onRewardedVideoFinished(double amount, string name)
//        {
//            if (rewardedVideoAdListener != null)
//                rewardedVideoAdListener.OnRewardedVideoFinished(amount,name);
//        }

//        public void onRewardedVideoClosed(bool finished)
//        {
//            if (rewardedVideoAdListener != null)
//                rewardedVideoAdListener.OnRewardedVideoClosed(finished);
//        }

//        public void onRewardedVideoExpired(){}


//        public override bool rewardVideoIsLoaded()
//        {
//           return Appodeal.isLoaded(Appodeal.REWARDED_VIDEO);
//        }

//        public void onRewardedVideoClicked()
//        {
//            //
//        }

//        public void onInterstitialShowFailed()
//        {
//           // throw new NotImplementedException();
//        }

//        public void onRewardedVideoShowFailed()
//        {
//          //  throw new NotImplementedException();
//        }
//    }
//}
