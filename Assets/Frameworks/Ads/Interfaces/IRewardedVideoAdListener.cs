﻿namespace Framework.Interfaces
{
    public interface IRewardedVideoAdListener
    {
        #region Rewarded Video callback handlers
        void OnRewardedVideoLoaded();
        void OnRewardedVideoShown();
        void OnRewardedVideoFailedToLoad();
        void OnRewardedVideoClosed(bool finished);
        void OnRewardedVideoFinished(double amount, string name);
        #endregion
    }
}
