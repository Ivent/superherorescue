﻿namespace Framework.Interfaces
{
    public interface ITick
    {
        void Tick();
    }
}
