﻿namespace Framework.Interfaces
{
    public interface IVibrationSystem
    {
        void Cancel();
        void VibrationOn();
        void VibrationOff();
        void LightVibrate();
        void MediumVibrate();
        void HardVibrate();
    }
}