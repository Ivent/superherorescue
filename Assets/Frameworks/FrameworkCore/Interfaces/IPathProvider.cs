﻿public interface IPathProvider
{
     string GetPath();
}