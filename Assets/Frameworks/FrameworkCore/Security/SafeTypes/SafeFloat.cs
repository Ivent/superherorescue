﻿using System;
namespace Framework.Security.SafeTypes
{
    public struct SafeFloat
    {
        private float offset;
        private float value;

        public SafeFloat(float value = 0)
        {
            Random random = new Random();
            offset = random.Next(-1000, +1000);
            this.value = value + offset;
        }

        public float GetValue()
        {
            return value - offset;
        }

        public void Dispose()
        {
            offset = 0;
            value = 0;
        }

        public override string ToString()
        {
            return GetValue().ToString();
        }

        public static SafeFloat operator +(SafeFloat f1, SafeFloat f2)
        {
            return new SafeFloat(f1.GetValue() + f2.GetValue());
        }

        public static SafeFloat operator -(SafeFloat f1, SafeFloat f2)
        {
            return new SafeFloat(f1.GetValue() - f2.GetValue());
        }

        public static SafeFloat operator /(SafeFloat f1, SafeFloat f2)
        {
            return new SafeFloat(f1.GetValue() / f2.GetValue());
        }

        public static SafeFloat operator *(SafeFloat f1, SafeFloat f2)
        {
            return new SafeFloat(f1.GetValue() * f2.GetValue());
        }

        public static SafeFloat operator %(SafeFloat f1, SafeFloat f2)
        {
            return new SafeFloat(f1.GetValue() % f2.GetValue());
        }

        public static bool operator ==(SafeFloat f1, SafeFloat f2)
        {
            return Math.Abs(f1.GetValue() - f2.GetValue()) < 0.01;
        }

        public static bool operator !=(SafeFloat f1, SafeFloat f2)
        {
            return Math.Abs(f1.GetValue() - f2.GetValue()) > 0.01;
        }

        public static bool operator <(SafeFloat f1, SafeFloat f2)
        {
            return f1.GetValue() < f2.GetValue();
        }

        public static bool operator >(SafeFloat f1, SafeFloat f2)
        {
            return f1.GetValue() < f2.GetValue();
        }

        public static bool operator <=(SafeFloat f1, SafeFloat f2)
        {
            return f1.GetValue() <= f2.GetValue();
        }

        public static bool operator >=(SafeFloat f1, SafeFloat f2)
        {
            return f1.GetValue() >= f2.GetValue();
        }
    }
}
