﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TouchCameraScroll : MonoBehaviour
{
    Camera mainCamera;
    [SerializeField] LayerMask collisionLayerMask;

    [SerializeField] LayerMask ingonreLayerMask;

    [SerializeField]
    SpriteRenderer boundsMap;

    Vector3 min;
    Vector3 max;

    Vector2 mousePos;
    Vector2 lastMousePos;

    Vector2 prevMousePos = new Vector2();
    bool clicked = false;

    void Start()
    {
        mainCamera = GetComponent<Camera>();
        CalculateBounds();
    }

    public void CalculateBounds()
    {
        if (boundsMap == null) return;
        Bounds bounds = Camera2DBounds();
        min = bounds.max + boundsMap.bounds.min;
        max = bounds.min + boundsMap.bounds.max;
    }

    Bounds Camera2DBounds()
    {
        float height = mainCamera.orthographicSize * 2;
        return new Bounds(Vector3.zero, new Vector3(height * mainCamera.aspect, height, 0));
    }

    bool PointOnBoundMap()
    {
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (lastMousePos != mousePos)
        {
            var origin = new Vector2(mousePos.x, mousePos.y);
            RaycastHit2D[] hits = Physics2D.RaycastAll(origin, Vector2.zero, 0f, collisionLayerMask);//.OrderBy(h => h.distance).ToArray();

            if (hits.Length == 1 )
            {
                return true;
            }
        }
        return false;
    }

    bool isIgnoredLayer()
    {
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (lastMousePos != mousePos)
        {
            var origin = new Vector2(mousePos.x, mousePos.y);
            RaycastHit2D[] hits = Physics2D.RaycastAll(origin, Vector2.zero, 0f, ingonreLayerMask);//.OrderBy(h => h.distance).ToArray();

            if (hits.Length == 1)
            {
                return true;
            }
        }
        return false;
    }

    void Update()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (Input.GetMouseButtonDown(0)  && PointOnBoundMap())
        {
            if(isIgnoredLayer() == false)
            {
               if( HelpFunc.IsPointerOverGameObject()  )
               {
                    return;
               }
            }
            prevMousePos = mousePos;
            clicked = true;

        }
        else if (Input.GetMouseButtonUp(0))
        {
            clicked = false;
        }

        if (clicked)
        {
            Vector3 pos = transform.position;
            pos += (Vector3)(prevMousePos - (Vector2)mousePos);
            pos.x = Mathf.Clamp(pos.x, min.x, max.x);
            pos.y = Mathf.Clamp(pos.y, min.y, max.y);
            transform.position = pos;
        }
    }
}
