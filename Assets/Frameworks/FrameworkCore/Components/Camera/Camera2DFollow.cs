﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Framework.Common.Camera
{

    public class Camera2DFollow : MonoBehaviour
    {
        [SerializeField] 
        UnityEngine.Camera targetCamera;

        [SerializeField]
        Transform target;

        [Range(0.01f, 10f)]
        public float Smooth = 2;//0.5f;

        Vector3 offset;

        Vector3 targetPos;

        [SerializeField] private SpriteRenderer boundsMap;

        private Vector3 min, max;

        bool isActive;

        public Vector3 BoarderMin { get => min; set => min = value; }
        public Vector3 BoarderMax { get => max; set => max = value; }

        private void Awake()
        {
            CalculateBounds();
        }

        public void CalculateBounds()
        {
            if (boundsMap == null) return;
            Bounds bounds = Camera2DBounds();
            min = bounds.max + boundsMap.bounds.min;
            max = bounds.min + boundsMap.bounds.max;
        }

        Bounds Camera2DBounds()
        {
            float height = targetCamera.orthographicSize * 2;
            return new Bounds(Vector3.zero, new Vector3(height * targetCamera.aspect, height, 0));
        }

        Vector3 MoveInside(Vector3 current, Vector3 pMin, Vector3 pMax)
        {
            if ( boundsMap == null) return current;
            current = Vector3.Max(current, pMin);
            current = Vector3.Min(current, pMax);
            return current;
        }


        public void SetTargetPos( Vector3 _targetPos )
        {
            targetPos = _targetPos;
            isActive = true;
        }

        public void SetTarget(Transform newTarget)
        {
            target = newTarget;
            offset = transform.position - target.position;
            isActive = true;
        }

        public void ClearTarget()
        {
            target = null;
            isActive = false;
        }

        void LateUpdate()
        {
            if (isActive)
            {

                if (target != null) targetPos = target.position;

                Vector3 moveVector = targetPos + offset;

                Vector3 moveVector2d = new Vector3 (moveVector.x, moveVector.y, transform.position.z);

                Vector3 position = MoveInside(moveVector2d, new Vector3(min.x, min.y, moveVector2d.z), new Vector3(max.x, max.y, moveVector2d.z));

                float dist = Vector3.Distance(transform.position, position);


                transform.position = Vector3.Slerp(transform.position, position, Time.deltaTime * (Smooth/dist)*3f);
            }
        }
    }
}
