﻿using UnityEngine.EventSystems;
using System.Linq;
using UnityEngine;
using Framework.Interfaces;

public class TouchDetector2D : MonoBehaviour {

    Vector2 mousePos;
    Vector2 lastMousePos;
    [SerializeField] LayerMask collisionLayerMask;
    [SerializeField] bool ignoreUI;

	void Update () {

        if (Input.GetMouseButtonDown(0) )
        {
        
            if (ignoreUI == true && HelpFunc.IsPointerOverGameObject() == true || Camera.main == null)
            {
                return;
            }

            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
 
                var origin = new Vector2(mousePos.x, mousePos.y);
                RaycastHit2D[] hits = Physics2D.RaycastAll(origin, Vector2.zero, 0f, collisionLayerMask).OrderBy(h => h.distance).ToArray();
               
                if (  hits.Length > 0 && hits[hits.Length-1].collider != null )
                {

                    IRayCastListiner listiner = hits[0].collider.GetComponent<IRayCastListiner>();
                    if (listiner != null)
                    {
                        listiner.RayCastClick();
   

                    }
                }
            
        }
    }

    
}
