﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ScalePulsationDotTween : MonoBehaviour
{

    [SerializeField] float endValue = 0.7f;
    [SerializeField] float duration = 0.4f;

     void StartAnimation()
    {
        transform.DOScale(endValue, duration).SetLoops(-1, LoopType.Yoyo);
    }
     void StopAnimation()
    {
        DOTween.Kill(transform);
    }

    private void OnEnable()
    {
        StartAnimation();
    }

    private void OnDisable()
    {
        StopAnimation();

    }
}
