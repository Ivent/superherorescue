using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextCounterAnim : MonoBehaviour {

	Text text;
	float defNumb;
	float currNumb;
	
	void Start () {
		text = GetComponent<Text> ();
		defNumb = float.Parse( text.text );
	}
	
	// Update is called once per frame
	void Update () {
		if (currNumb < defNumb) {
			currNumb ++;
			text.text = Mathf.Round( currNumb ).ToString ();
		} else
			enabled = false;
	
	}
}
