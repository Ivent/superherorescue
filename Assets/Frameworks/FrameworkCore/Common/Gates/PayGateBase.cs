﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PayGateBase : MonoBehaviour, IGate
{
    ValueModelBaseLong valueModel;

    [SerializeField]
    protected string gateId;

    [SerializeField]
    public long reqValue;

    public long ReqValue
    {
        get
        {
            return reqValue;
        }
        set
        {
            reqValue = value;
            ShowReqValueText(reqValue);
        }
    }

    public Action OnUnlocked = delegate { };

    public virtual void Init(ValueModelBaseLong _valueModelBase)
    {
        valueModel = _valueModelBase;

        if (IsUnlocked())
            return;

        valueModel.OnValueChanged += ValueModel_OnValueChanged;
        ShowReqValueText(reqValue);
    }

    void ValueModel_OnValueChanged()
    {
        if(IsAvaible())
        {
            ShowAvaibleStatus();
        }
        else
        {
            ShowUnAvaibleStatus();
        }
    }


    public void TryUnlock()
    {

        if (gameObject.activeInHierarchy == false) return;
        if (IsUnlocked()) return;


        if (IsAvaible())
        {
            valueModel.DecValue(reqValue);
            PlayerPrefs.SetInt("gateIdUnlockStatus"+gateId, 1);

            gameObject.SetActive(false);
            OnUnlocked();
        }
    }

     bool IsAvaible()
    {
        return reqValue <= valueModel.CurrentValue;
    }

    public bool IsUnlocked()
    {
        return (PlayerPrefs.GetInt("gateIdUnlockStatus"+ gateId) == 1);
    }

    public abstract void ShowAvaibleStatus();
    public abstract void ShowUnAvaibleStatus();
    public abstract void ShowReqValueText(long reqValue);

    void OnDestroy()
    {
        valueModel.OnValueChanged -= ValueModel_OnValueChanged; ;
    }
}
