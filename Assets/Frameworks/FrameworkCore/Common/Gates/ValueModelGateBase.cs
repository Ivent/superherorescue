﻿using System;
using UnityEngine;

public abstract class ValueModelGateBase : MonoBehaviour, IGate
{
     ValueModelBaseInt valueModel;

    [SerializeField]
    public int reqValue;
    public int ReqValue
    {
        get
        {
            return reqValue;
        }
        set
        {
            reqValue = value;
            ShowReqValueText(reqValue);
        }
    }

    public Action OnUnlocked = delegate {};
     
    public virtual void Init(ValueModelBaseInt _valueModelBase)
    {
        valueModel = _valueModelBase;
        valueModel.OnValueChanged += StarModel_OnValueChanged;
        TryUnlock();
        ShowReqValueText(reqValue);

    }

   public abstract void ShowReqValueText(int reqValue);

    void StarModel_OnValueChanged()
    {
        TryUnlock();
    }

   public void TryUnlock()
    {

        if (gameObject.activeInHierarchy == false) return;

        if (IsUnlocked())
        {
            gameObject.SetActive(false);
            OnUnlocked();
        }
    }

    public bool IsUnlocked()
    {
        return reqValue <= valueModel.CurrentValue;
    }

    void OnDestroy()
    {
        valueModel.OnValueChanged -= StarModel_OnValueChanged;
    }
}
