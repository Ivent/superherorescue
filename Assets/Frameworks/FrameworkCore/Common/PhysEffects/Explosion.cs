﻿using Framework.Utility;
using UnityEngine;

namespace Framework.Components
{
    [RequireComponent(typeof(CircleCollider2D),typeof(ExplosiveActivator))]
    public class Explosion : MonoBehaviour
    {

        [SerializeField] float explosion_force;
        [SerializeField] float explosion_rate;
        [SerializeField] float explosion_max_sixe;
        [SerializeField] float currentRadius;

        ExplosiveActivator explosiveActivator;

        bool exploded;

        Rigidbody2D rb;
        CircleCollider2D expRadius;

        public float Explosion_max_sixe { get => explosion_max_sixe; set => explosion_max_sixe = value; }

        public void Awake()
        {
            expRadius = GetComponent<CircleCollider2D>();
            explosiveActivator = GetComponent<ExplosiveActivator>();

            explosiveActivator.OnBoom += ExplosiveActivator_OnBoom;
        }

        void ExplosiveActivator_OnBoom()
        {
            Boom();
        }

        private void FixedUpdate()
        {
            if (exploded)
            {
                if (currentRadius < Explosion_max_sixe)
                {
                    currentRadius += explosion_rate;
                }
                else
                {
                    Destroy(gameObject);
                }
                expRadius.radius = currentRadius;
            }
        }

         void Boom()
        {
            exploded = true;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            Rigidbody2D rb;
            if (exploded)
            {
                rb = collision.gameObject.GetComponent<Rigidbody2D>();
                if (rb != null)
                {
                    rb.AddExplosionForce2D( explosion_force, transform.position, currentRadius);
                }
            }
        }

        private void OnDestroy()
        {
            explosiveActivator.OnBoom -= ExplosiveActivator_OnBoom;
        }
    }
}
