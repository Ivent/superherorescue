﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolObject : MonoBehaviour
{
    public string poolKey;
    public float lifeTime = 60;

    public virtual void OnAwake()
    {
        //        Debug.Log("PoolObject Awake");
        Done(lifeTime);
    }

    protected void Done(float time)
    {
        Invoke("Done", time);
    }

    protected void Done()
    {
        PoolManager.instance.ReturnObjectToQueue(gameObject);
    }
}