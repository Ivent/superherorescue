﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CountdownEffect : MonoBehaviour
{
   protected bool countdownIsStarted;
   public float countdownTime;

    public void StartEffect(float _time)
    {
        if(_time > countdownTime)
        countdownTime = _time;

        if (!countdownIsStarted)
        {
            ApplyEffect();
            countdownIsStarted = true;
        }
    }

    protected virtual void Start()
    {

    }

    public abstract void ApplyEffect();
    public abstract void ResetEffect();


    public void CountDownEnd()
    {
        countdownIsStarted = false;
        ResetEffect();
    }


    private void Update()
    {
        if(countdownIsStarted )
        {
            countdownTime -= Time.deltaTime;
            if(countdownTime <= 0)
            {
                CountDownEnd();
            }
        }
    }
}
