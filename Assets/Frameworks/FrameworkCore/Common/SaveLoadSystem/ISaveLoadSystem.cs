﻿using System;
using System.Collections.Generic;

public interface ISaveLoadSystem
{
    float GetFloat(string _key, float _defValue = 0);
    int GetInt(string _key, int _defValue = 0);
    string GetString(string _key, string _defValue = "");
    bool IsSaved(string _key);
    void SaveStruct(string _key, ValueType valueType);
    T LoadStruct<T>(string _key);
    void SaveClass(string _key, object classType);
    T LoadClass<T>(string _key);
    void SetFloat(string _key, float _value);
    void SetInt(string _key, int _value);
    void SetString(string _key, string _value);
}