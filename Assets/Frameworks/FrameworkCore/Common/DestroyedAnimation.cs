using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyedAnimation : MonoBehaviour
{
    [SerializeField]
    bool destroyAftherEnd = true;

    public void AnimationEnd()
    {
        if (destroyAftherEnd)
        {
            Destroy(gameObject);
        }

    }
}
