using System;
using Adic;
using Framework.Interfaces;
using Framework.Managers.Event;
using UnityEngine;

public class LevelModel : MonoBehaviour, IDisposable
{
    [Inject] EventDispatcher eventManager;


    bool LevelisReadyToUp;

    [Inject]
    public void Init()
    {
        Load();
      //  eventManager.AddListener(EVENT_TYPE.CHARACTERS_CHANGED, this);
    }

 

   public float exp;
    public float Exp
    {
        get
        {
            return exp;
        }
          private set
        {

            exp = value;
        }
    }

  public  float maxExp;
    public float MaxExp
    {
        get
        {
            return maxExp;
        }
        private set
        {
            maxExp = value;
        }
    }

    int currentLevel;
    public int CurrentLevel
    {
        get
        {
            return currentLevel;
        }
        set
        {
            currentLevel = value;
        }
    }

    public void Save()
    {
        PlayerPrefs.SetInt("currentLevel", CurrentLevel);
        PlayerPrefs.SetFloat("exp", Exp);
    }

    public void Load()
    {
        Exp = PlayerPrefs.GetFloat("exp");
        CurrentLevel = PlayerPrefs.GetInt("currentLevel", 1);
        maxExp = GetMaxExpValue();
    }


    public float getLevelProgressRatio()
    {
        return (float)Exp / MaxExp;
    }

    public void AddExp(long _exp)
    {
        if (Exp+_exp >= MaxExp)
        {
            LevelReadyToUp();
        }

        if(Exp < MaxExp)
        {
            Exp += _exp;
            eventManager.PostNotification("EVENT_TYPE.EXP_GROW", null);
        }
        Save();
    }

    public void DecExp(long _exp)
    {
        if(Exp > 0)
        {
            Exp -= _exp;
            eventManager.PostNotification("EVENT_TYPE.EXP_FALL", null);
        }
        else
        {
            Exp = 0;
        }

    }

    public LevelProgressData GetProgressData()
    {
        return new LevelProgressData(Exp, MaxExp, CurrentLevel);
    }

    void LevelReadyToUp()
    {
        //if (!LevelisReadyToUp)
        //{
        //    LevelisReadyToUp = true;
        //    eventManager.PostNotification(EVENT_TYPE.LEVEL_READY_UP, null);

        //}
        LevelUp();
    }

   public void LevelUp()
    {
        LevelisReadyToUp = false;
        Exp = 0;
        CurrentLevel++;
        MaxExp = GetMaxExpValue();
        eventManager.PostNotification("EVENT_TYPE.LEVEL_UP", null );
        Save();
    }

    int GetMaxExpValue()
    {
       
        return 10 * currentLevel;

    }

    public void Dispose()
    {
        eventManager = null;
    }

}


public class LevelProgressData
{
    public int CurrentLevel;
    public float Exp, MaxExp;

    public LevelProgressData(float exp, float maxExp, int currentLevel)
    {
        Exp = exp;
        MaxExp = maxExp;
        CurrentLevel = currentLevel;
    }


}
