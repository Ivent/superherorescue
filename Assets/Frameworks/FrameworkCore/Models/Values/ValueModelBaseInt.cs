﻿using System;
using Adic;

using UnityEngine;

public class ValueModelBaseInt
{
     string valueId;
     int defStartValue = 200;
     int defMaxValue = 999999;

    public Action OnValueChanged = delegate {};
    public Action OnNotEnoughValues = delegate {};

    int currentValue;
    public int CurrentValue
    {
        get
        {
            return currentValue;
        }
       private set
        {
            currentValue = value;
            OnValueChanged();
           
        
        }

    }

     int maxValue;
    public int MaxValue
    {
        get
        {
            return maxValue;
        }
      private  set
        {
            maxValue = value;
       
        }
    }

    public void AddValue(int _value)
    {
        CurrentValue += _value;

        if (currentValue > MaxValue) CurrentValue = MaxValue;
    }

    public bool DecValue(int _value)
    {
        if (!isValueEnough(_value))
        {
            OnNotEnoughValues();
            return false;
        }

        CurrentValue -= _value;

        return true;
    }


    public  void Init(string _valueId,int _defStartValue, int _defMaxValue)
    {
        valueId = _valueId;
        defStartValue = _defStartValue;
        defMaxValue = _defMaxValue;

        Load();

    }

    public bool isValueEnough(int _cost)
    {
        return (currentValue - _cost) >= 0;
    }

    public void Save()
    {
        PlayerPrefs.SetInt("current" + valueId, currentValue);
        PlayerPrefs.SetInt("max" + valueId, maxValue);
    }

    public void Load()
    {
        CurrentValue = PlayerPrefs.GetInt("current" + valueId, defStartValue);
        MaxValue = PlayerPrefs.GetInt("max" + valueId, defMaxValue);
    }
}

