﻿using System;
using Adic;

using UnityEngine;

public class ValueModelBaseLong
{
     string valueId;
     long defStartValue = 200;
     long defMaxValue = 999999;

    public Action OnValueChanged = delegate {};
    public Action OnNotEnoughValues = delegate {};

    long currentValue;
    public long CurrentValue
    {
        get
        {
            return currentValue;
        }
       private set
        {
            currentValue = value;
            OnValueChanged();
           
        
        }

    }

     long maxValue;
    public long MaxValue
    {
        get
        {
            return maxValue;
        }
      private  set
        {
            maxValue = value;
       
        }
    }

    public void AddValue(long _value)
    {
        CurrentValue += _value;

        if (currentValue > MaxValue) CurrentValue = MaxValue;
    }

    public bool DecValue(long _value)
    {
        if (!isValueEnough(_value))
        {
            OnNotEnoughValues();
            return false;
        }

        CurrentValue -= _value;

        return true;
    }

    public bool isValueEnough(long _cost)
    {
        return (currentValue - _cost) >= 0;
    }


    public void Init(string _valueId, long _defStartValue, long _defMaxValue)
    {
        valueId = _valueId;
        defStartValue = _defStartValue;
        defMaxValue = _defMaxValue;

        Load();

    }

    public void Save()
    {
        PlayerPrefs.SetString("current" + valueId, currentValue.ToString());
        PlayerPrefs.SetString("max" + valueId, maxValue.ToString());
    }

    public void Load()
    {
        CurrentValue = Convert.ToInt64( PlayerPrefs.GetString("current" + valueId, defStartValue.ToString()));
        MaxValue = Convert.ToInt64( PlayerPrefs.GetString("max" + valueId, defMaxValue.ToString()));
    }
}

