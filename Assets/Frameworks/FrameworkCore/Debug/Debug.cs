﻿using System.Diagnostics;

namespace Assets.Scripts.Unity
{
  /// <summary>
  /// Class used to show User logs only in Editor.
  /// </summary>
	public static class Debug
	{
	  private const string ConditionalSymbol = "UNITY_EDITOR";

    /// <summary>
    /// Logs message with parameters in string.Format format type.
    /// </summary>
    /// <param name="messageFormat">Message format</param>
    /// <param name="additionalParams">Additional parameters</param>
    [Conditional(ConditionalSymbol)]
    public static void Log(string messageFormat, params object[] additionalParams)
    {
      if (UnityEngine.Debug.isDebugBuild)
        UnityEngine.Debug.Log(string.Format(messageFormat, additionalParams));
    }

    /// <summary>
    /// Logs any object.
    /// </summary>
    /// <param name="obj"></param>
    [Conditional(ConditionalSymbol)]
    public static void Log(object obj)
    {
      if (UnityEngine.Debug.isDebugBuild)
        UnityEngine.Debug.Log(obj);
    }

    /// <summary>
    /// Logs warning with parameters in string.Format format type.
    /// </summary>
    /// <param name="messageFormat">Message format</param>
    /// <param name="additionalParams">Additional parameters</param>
    [Conditional(ConditionalSymbol)]
    public static void LogWarning(string messageFormat, params object[] additionalParams)
    {
      if (UnityEngine.Debug.isDebugBuild)
        UnityEngine.Debug.LogWarning(string.Format(messageFormat, additionalParams));
    }

    /// <summary>
    /// Logs error with parameters in string.Format format type.
    /// </summary>
    /// <param name="messageFormat">Message format</param>
    /// <param name="additionalParams">Additional parameters</param>
    [Conditional(ConditionalSymbol)]
    public static void LogError(string messageFormat, params object[] additionalParams)
    {
      if (UnityEngine.Debug.isDebugBuild)
        UnityEngine.Debug.LogError(string.Format(messageFormat, additionalParams));
    }
  }
}
