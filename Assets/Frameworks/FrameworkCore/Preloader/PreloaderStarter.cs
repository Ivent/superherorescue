﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using Framework.Managers.States;
using UnityEngine;

public class PreloaderStarter : MonoBehaviour
{
    [SerializeField] string nextSceneName;

    [Inject] StateManager stateManager;

    void Start()
    {
        this.Inject();

        stateManager.ChangeScene(nextSceneName);

    }
}
