﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using Framework.Common;
using Framework.Controllers.Loader;
using Framework.Managers.States;
using Framework.Views;
using UnityEngine;

public class PreloaderContext : BaseMainContext
{
    [SerializeField] LoaderView loaderView;

    protected override void BindCommands()
    {

    }

    protected override void BindComponents()
    {

    }

    protected override void BindConfigs()
    {

    }

    protected override void BindControllers()
    {
        this.containers[0]
             .Bind<ISceneLoader>().ToGameObject<ControllerAsyncLoader>()
             .Bind<StateManager>().ToGameObject<StateManager>();
                    
    }

    protected override void BindManagers()
    {

    }

    protected override void BindModels()
    {

    }

    protected override void BindView()
    {
        this.containers[0]
         .Bind<LoaderView>().To(loaderView);
    }
}
