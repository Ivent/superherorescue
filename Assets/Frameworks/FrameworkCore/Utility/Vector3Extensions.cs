﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using SystemMath = System.Math;

namespace Assets.Scripts.System.Math
{
  /// <summary>
  /// Extended functions for Unity's Vector3 class.
  /// </summary>
	public static class Vector3Extensions
	{
    /// <summary>
    /// Calculates center point for collection of points or zero-point if no points available.
    /// </summary>
    /// <param name="points">Collection of points</param>
    /// <returns>Center point or zero</returns>
    public static Vector3 Centre(params Vector3[] points)
    {
      return CentreOrDefault(Vector3.zero, points);
    }

    /// <summary>
    /// Calculates center point for collection of points or some specified default-point if no points available.
    /// </summary>
    /// <param name="defaultPoint">Default point to return if no points available</param>
    /// <param name="points">Collection of points</param>
    /// <returns>Center point or default point</returns>
    public static Vector3 CentreOrDefault(Vector3 defaultPoint, ICollection<Vector3> points)
    {
      return CentreOrDefault(defaultPoint, points, point => point);
    }

    /// <summary>
    /// Calculates center point for collection of objects using point selector.
    /// </summary>
    /// <param name="defaultPoint">Default point to return if no points available</param>
    /// <param name="objects">Collection of objects</param>
    /// <param name="selector">Reterns point from the object</param>
    /// <returns>Center point or default point</returns>
    public static Vector3 CentreOrDefault<T>(Vector3 defaultPoint, ICollection<T> objects, Func<T, Vector3> selector)
    {
      if (objects.Count == 0)
        return defaultPoint;
      Vector3 sum = Vector3.zero;
      foreach (var o in objects)
        sum = sum + selector(o);
      return sum / objects.Count;
    }

    /// <summary>
    /// Clamps vector from min to max value by its axis.
    /// </summary>
    /// <param name="val">Value to clamp</param>
    /// <param name="min">Min axis-values</param>
    /// <param name="max">Max axis-values</param>
    /// <returns>Clamped vector</returns>
    public static Vector3 Clamp(this Vector3 val, Vector3 min, Vector3 max)
    {
      max = Vector3.Max(min, max); // max must be greater or equal min.
      val = Vector3.Max(min, val);
      val = Vector3.Min(max, val);
      return val;
    }

    /// <summary>
    /// Clamps Vector of angles.
    /// </summary>
    public static Vector3 ClampAngle(this Vector3 alpha, Vector3 min, Vector3 max)
    {
      return new Vector3(AngleHelper.Clamp(alpha.x, min.x, max.x),
                         AngleHelper.Clamp(alpha.y, min.y, max.y),
                         AngleHelper.Clamp(alpha.z, min.z, max.z));
    }

    /// <summary>
    /// Returns random point in specified area centered in world's center.
    /// </summary>
    /// <param name="size">Size of area</param>
    /// <returns>Random point in area</returns>
    public static Vector3 RandomInArea(Vector3 size)
    {
      Vector3 pos = Vector3.zero;
      pos.x = Random.Range(-size.x / 2, size.x / 2);
      pos.y = Random.Range(-size.y / 2, size.y / 2);
      pos.z = Random.Range(-size.z / 2, size.z / 2);
      return pos;
    }

    /// <summary>
    /// Vector with absolute value for each coordinate.
    /// </summary>
    /// <param name="val">Source vector</param>
    /// <returns>Vector with absolute value for each coordinate</returns>
    public static Vector3 Abs(this Vector3 val)
    {
      return new Vector3(SystemMath.Abs(val.x), SystemMath.Abs(val.y), SystemMath.Abs(val.z));
    }
	}
}
