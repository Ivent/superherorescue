using System;

namespace Assets.Scripts.System.Math
{
  /// <summary>
  /// Implementation of simple random based on .NET Random.
  /// </summary>
  public class SimpleRandom 
  {
    private readonly Random random;

    public SimpleRandom()
    {
      random = new Random();
    }

    public SimpleRandom(int seed)
    {
      random = new Random(seed);
    }

    #region Implementation of IRandom

    public float Range(float min, float max)
    {
      float d = max - min;
      double rnd = random.NextDouble();
      return (float)(min + d*rnd);
    }

    public float Range01()
    {
      return (float)random.NextDouble();
    }

    public int Range(int min, int max)
    {
      return random.Next(min, max);
    }

    public bool Boolean
    {
      get { return random.Next(int.MinValue, int.MaxValue) > 0; }
    }

    public int Integer
    {
      get { return random.Next(); }
    }

    #endregion
  }
}
