﻿using UnityEngine;

namespace Assets.Scripts.System.Math
{
	public static class AngleHelper
	{
    public const int Pi = 180;
    public const int DoublePi = Pi*2;

    /// <summary>
    /// Clamps angle in given interval.
    /// </summary>
    /// <param name="angle">Angle to clamp</param>
    /// <param name="min">Minimum of the interval.</param>
    /// <param name="max">Maximum of the interval.</param>
    /// <returns>Clamped angle converted to interval [0, 360].</returns>
    public static float Clamp(float angle, float min, float max)
    {
      if (max - min > DoublePi) //interval size is greater than 360.
        return angle;
      // normalizing minimum to [-180, 180]
      float normMin = NormalizeNegative(min);
      max -= min - normMin;
      min = normMin;
      return Mathf.Clamp(min < 0 ? NormalizeNegative(angle) : NormalizePositive(angle), min, max);
    }

	  /// <summary>
    /// Converts angle to [0, 360]
    /// </summary>
    public static float NormalizePositive(float angle)
    {
      while (Mathf.Abs(angle) > DoublePi)
        angle -= Mathf.Sign(angle)*DoublePi;
      return angle < 0 ? angle + DoublePi : angle;
    }

    /// <summary>
    /// Converts angle to [-180, 180]
    /// </summary>
    public static float NormalizeNegative(float angle)
    {
      while (Mathf.Abs(angle) >= DoublePi)
        angle -= Mathf.Sign(angle) * DoublePi;
      return angle >= Pi ? angle - DoublePi : angle;
    }

	}
}
