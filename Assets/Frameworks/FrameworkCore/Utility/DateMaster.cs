using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DateMaster 
{
    
    public static int NowSeconds()
    {
        DateTime startTime = new DateTime( 2014, 01, 01 );
        TimeSpan span = new TimeSpan( DateTime.Now.Ticks - startTime.Ticks );
        return ( int ) span.TotalSeconds;
    }
    
    public static void SaveDate( DateTime  _date, string _saveKey)
    {
        PlayerPrefs.SetString(_saveKey, _date.ToBinary().ToString());
    }

    public static DateTime GetDate(string _saveKey)
    {
        if( !PlayerPrefs.HasKey( _saveKey))
        {
            return DateTime.Now;
        }
        long temp = Convert.ToInt64(PlayerPrefs.GetString(_saveKey));
        return DateTime.FromBinary(temp);
    }

    public  static TimeSpan GetDifference( DateTime _oldDate)
    {
       return DateTime.Now.Subtract(_oldDate);
    }

    public static string ToCustomString(this TimeSpan _elapsedTime)
    {
        int elapsedHours = Mathf.Abs(_elapsedTime.Hours);
        int elapsedMinutes = Mathf.Abs(_elapsedTime.Minutes);
        int elapsedSeconds = Mathf.Abs(_elapsedTime.Seconds);
        return string.Format("{0:00}:{1:00}:{2:00}", elapsedHours, elapsedMinutes, elapsedSeconds);
    }

    public static string GetFormatedTimeSpan(TimeSpan _elapsedTime)
    {
        int elapsedHours = Mathf.Abs(_elapsedTime.Hours);
        int elapsedMinutes = Mathf.Abs(_elapsedTime.Minutes);
        int elapsedSeconds = Mathf.Abs(_elapsedTime.Seconds);

        if ( elapsedHours > 0)
        {
            return string.Format("{0:D2} ч, {1:D2} мин, {2:D2} сек", elapsedHours, elapsedMinutes, elapsedSeconds);

        }

        if(elapsedMinutes > 0 )
        {
            return  string.Format("{0:D2} мин, {1:D2} сек", elapsedMinutes, elapsedSeconds);

        }

        if (elapsedSeconds >= 0)
        {
            return string.Format("{0:D2} сек", elapsedSeconds);

        }
        throw new ArgumentNullException();
    }
}
