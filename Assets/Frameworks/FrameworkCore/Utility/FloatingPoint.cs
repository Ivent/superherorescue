﻿namespace Assets.Scripts.System.Math
{
	public static class FloatingPoint
	{
	  public const float Epsilon = 0.000000000000001f;

    public static bool Approximately(float a, float b)
    {
      return global::System.Math.Abs(a - b) < Epsilon;
    }
	}
}
