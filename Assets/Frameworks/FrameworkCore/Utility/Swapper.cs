﻿namespace Assets.Scripts.System.Math
{
  /// <summary>
  /// Swapes values of two objects.
  /// </summary>
	public static class Swapper
	{
    /// <summary>
    /// Generic version of swap.
    /// </summary>
    /// <typeparam name="T">Type of objects</typeparam>
    /// <param name="x">first object</param>
    /// <param name="y">second object</param>
    public static void Swap<T>(ref T x, ref T y)
    {
      T a = x;
      x = y;
      y = a;
    }

    /// <summary>
    /// Special function for int values.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    public static void Swap(ref int x, ref int y)
    {
      x = x + y;
      y = x - y;
      x = x - y;
    }

    /// <summary>
    /// Special function for float values.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    public static void Swap(ref float x, ref float y)
    {
      x = x + y;
      y = x - y;
      x = x - y;
    }
  }
}
