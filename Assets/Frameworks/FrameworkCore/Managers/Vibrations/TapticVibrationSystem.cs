﻿using UnityEngine;
using System.Collections;
using Adic;
using Framework.Interfaces;

public class TapticVibrationSystem : IVibrationSystem
{


    public void LightVibrate()
    {
        if (GlobalVars.Vibration == 1 && HasVibrator())
        {
            Taptic.Light();
        }
    }

    public void MediumVibrate()
    {
        if (GlobalVars.Vibration == 1 && HasVibrator())
        {
            Taptic.Medium();
        }
    }

    public void HardVibrate()
    {
        if (GlobalVars.Vibration == 1 && HasVibrator())
        {
            Taptic.Heavy();
        }
    }


    bool HasVibrator()
    {
        return true;
    }

    public void Cancel()
    {
      ///
    }

    public void VibrationOn()
    {
        LightVibrate();
        Taptic.tapticOn = true;
    }

    public void VibrationOff()
    {
        Taptic.tapticOn = false; 
    }
}