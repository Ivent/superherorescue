﻿using UnityEngine;
using System.Collections;
using Adic;
using Framework.Interfaces;

public class VibrationSystem : IVibrationSystem
{

    public AndroidJavaClass unityPlayer;
    public AndroidJavaObject currentActivity;
    public AndroidJavaObject sysService;

    [Inject]
    public VibrationSystem()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        sysService = currentActivity.Call<AndroidJavaObject>("getSystemService", "vibrator");
#endif


    }

    //Functions from https://developer.android.com/reference/android/os/Vibrator.html

    public void Cancel()
    {
        if (GlobalVars.Vibration == 1 && HasVibrator())
            sysService.Call("cancel");
    }

    bool HasVibrator()
    {
        if (isAndroid())
            return sysService.Call<bool>("hasVibrator");
        else return false;
    }

    private bool isAndroid()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
    return true;
#else
        return false;
#endif
    }

    public void LightVibrate()
    {
        if (GlobalVars.Vibration == 1 && HasVibrator())
            sysService.Call("vibrate",50);
    }

    public void MediumVibrate()
    {
        if (GlobalVars.Vibration == 1 && HasVibrator())
            sysService.Call("vibrate");
    }

    public void HardVibrate()
    {
        if (GlobalVars.Vibration == 1 && HasVibrator())
            sysService.Call("vibrate",200);
    }

    public void VibrationOn()
    {
        LightVibrate();
    }

    public void VibrationOff()
    {
        Cancel();
    }
}