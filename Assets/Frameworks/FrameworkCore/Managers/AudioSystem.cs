﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SoundEnum { BG_MUSIC,ACTION_SOUND,COIN_TAP_SOUND,FOUND_STAR_SOUND };

public class AudioSystem : MonoBehaviour, IAudioSystem
{

    [SerializeField] AudioSource bgMusicAudioSource;
    [SerializeField] AudioSource soundAudioSource;

    [SerializeField] AudioClip bgMusic;

    [SerializeField] AudioClip actionSound;
    [SerializeField] AudioClip coinTapSound;
    [SerializeField] AudioClip newStarSound;


    bool isMuted;

    private void Start()
    {
    }

    public void Mute()
    {
        isMuted = true;
        UpdSoundState();
    }

    public void Unmute()
    {
        isMuted = false;
        UpdSoundState();
    }

    void UpdSoundState()
    {
        if (isMuted)
        {
            bgMusicAudioSource.volume = 0;
            soundAudioSource.volume = 0;
        }
        else
        {
            bgMusicAudioSource.volume = 1;
            soundAudioSource.volume = 1;
        }
    }

    public void PlayBgMusic(SoundEnum musicType )
    {

        bgMusicAudioSource.clip = GetAudioClip(musicType);
        bgMusicAudioSource.Play();
    }

    public void PlayOnce(SoundEnum soundType)
    {
        soundAudioSource.PlayOneShot(GetAudioClip(soundType));
    }

    AudioClip GetAudioClip(SoundEnum soundEnum)
    { 
        switch ( soundEnum)
        {
            case SoundEnum.BG_MUSIC:
                return bgMusic;
            case SoundEnum.ACTION_SOUND:
                return actionSound;
            case SoundEnum.COIN_TAP_SOUND:
                return coinTapSound;
            case SoundEnum.FOUND_STAR_SOUND:
                return newStarSound;

        }
        return null;
    }
}
