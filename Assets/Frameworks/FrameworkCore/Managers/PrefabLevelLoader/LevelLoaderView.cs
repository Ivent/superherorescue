﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using Framework;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class LevelLoaderView : MonoBehaviour
{
    [Inject] GameLevelController levelLoader;

    [SerializeField] GameObject visual;

    [SerializeField] Image loadingImage;

    private void Awake()
    {
        visual.SetActive(true);
    }

    private void Start()
    {
        this.Inject();

        levelLoader.OnLoadFinished += LevelLoader_OnLoadFinished;
        levelLoader.OnLoadStarted += LevelLoader_OnLoadStarted;

    }
    

    void LevelLoader_OnLoadFinished()
    {
        loadingImage.DOFade(0, 0.5f).OnComplete(() =>
        {
            visual.SetActive(false);
        });

    }

    void LevelLoader_OnLoadStarted()
    {
        loadingImage.DOFade(1, 0f);
        visual.SetActive(true);
    }

    private void OnDestroy()
    {
        levelLoader.OnLoadFinished -= LevelLoader_OnLoadFinished;
        levelLoader.OnLoadStarted -= LevelLoader_OnLoadStarted;
    }
}
