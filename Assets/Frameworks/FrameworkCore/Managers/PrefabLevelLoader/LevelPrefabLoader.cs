﻿using System;
using System.Collections;
using UnityEngine;

namespace Framework
{
    public class LevelPrefabLoader : MonoBehaviour, ILoader
    {
        string levelFolderName = "[LEVELS]";
        Transform levelFolder;

        public Action OnLoadFinished { get; set; }
        public Action OnLoadStarted { get; set; }

       float loadingDelay = 0.5f;

        private void Awake()
        {
            levelFolder = GameObject.Find(levelFolderName).transform;
        }

        IEnumerator ClearLevelFolderCorun()
        {
            foreach (Transform child in levelFolder)
            {
                Destroy(child.gameObject);
            }

            yield return null;

        }

        public void LoadLevel(IPathProvider _pathProvider)
        {
            StartCoroutine(ClearAndLoadLevelCorun(_pathProvider));
        }

        IEnumerator ClearAndLoadLevelCorun(IPathProvider _pathProvider)
        {
            yield return StartCoroutine(ClearLevelFolderCorun());
            OnLoadStarted?.Invoke();

            yield return new WaitForSeconds(loadingDelay);

            yield return StartCoroutine(LoadLevelCorun(_pathProvider));
            OnLoadFinished?.Invoke();

        }

        IEnumerator LoadLevelCorun(IPathProvider _pathProvider)
        {
            string pathToResource = _pathProvider.GetPath();
            yield return Instantiate(Resources.Load(pathToResource), levelFolder);
        }
    }
}
