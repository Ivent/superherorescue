﻿using System;

namespace Framework
{
    public interface ILoader
    {
        Action OnLoadFinished { get; set; }
        Action OnLoadStarted { get; set; }
    }
}