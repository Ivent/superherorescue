﻿using System.Collections.Generic;
using Framework.Common;
using Framework.Interfaces;
using UnityEngine;

namespace Framework.Managers.Event
{
    public class EventDispatcher:MonoBehaviour
    {
        private Dictionary<string, List<IEventDispatcherListenier>> Listeners 
        = new Dictionary<string, List<IEventDispatcherListenier>>();

        public void AddListener(string EventCode, IEventDispatcherListenier Listener)
        {
            List<IEventDispatcherListenier> ListenList = null;

            if (Listeners.TryGetValue(EventCode,out ListenList))
            {
                ListenList.Add(Listener);
                return;
            }

            ListenList = new List<IEventDispatcherListenier>();
            ListenList.Add(Listener);
            Listeners.Add(EventCode, ListenList);
        }

        public void PostNotification(string EventCode, Component Sender, Object Param = null)
        {
            List<IEventDispatcherListenier> ListenList = null;

            if (!Listeners.TryGetValue(EventCode, out ListenList))
                return;

            for ( int i = 0; i < ListenList.Count; i++ )
            {
                if (!ListenList[i].Equals(null))
                    ListenList[i].OnEvent(EventCode, Sender, Param);
            }
        }

        public void RemoveEvent(string EventCode)
        {
            Listeners.Remove(EventCode);
        }

        public void RemoveRedundancies()
        {
            Dictionary<string, List<IEventDispatcherListenier>> TmpListeners  = new Dictionary<string, List<IEventDispatcherListenier>>();

            foreach(KeyValuePair<string,List<IEventDispatcherListenier>> Item in Listeners )
            {
                for ( int i = Item.Value.Count-1; i >0; i--)
                {
                    if (Item.Value[i].Equals(null))
                        Item.Value.RemoveAt(i);

                    if (Item.Value.Count > 0)
                        TmpListeners.Add(Item.Key, Item.Value);
                }
            }
            Listeners = TmpListeners;
        }

        private void OnLevelWasLoaded(int level)
        {
            RemoveRedundancies();
        }
    }
}
