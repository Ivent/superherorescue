﻿using Framework.Interfaces;
using UnityEngine;

namespace Framework.Managers.Event
{
    public interface IEventDispatcher
    {
        void AddListener(string EventCode, IEventDispatcherListenier Listener);
        void PostNotification(string EventCode, Component Sender, System.Object Param = null);
        void RemoveEvent(string EventCode);
        void RemoveRedundancies();
    }
}