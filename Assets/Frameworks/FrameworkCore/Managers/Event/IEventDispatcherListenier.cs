﻿using UnityEngine;

namespace Framework.Interfaces
{

    public interface IEventDispatcherListenier
    {
        void OnEvent(string EventCode, Component Sender, Object Param = null);
    }
}
