﻿using UnityEngine;
using UnityEngine.UI;

namespace Framework.Views
{ 
    public class SliderView : BaseFloatValueView
    {
        [SerializeField]
        private Slider _slider;
        public override void SetNormalizedValue(float value)
        {
            _slider.value = (_slider.maxValue - _slider.minValue) * value;
        }
    }
}
