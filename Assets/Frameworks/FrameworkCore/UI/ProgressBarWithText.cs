﻿using System.Collections;
using System.Collections.Generic;
using Framework.Views;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBarWithText : MonoBehaviour, IProgressBar
{
    [SerializeField]
    Text percentFillingValue;

    [SerializeField]
    FillingImageView fillingImageView;

    public void FillValue(float minValue, float maxValue )
    {
        percentFillingValue.text = minValue.ToString() + " / " + maxValue.ToString();
        fillingImageView.SetNormalizedValue(minValue / maxValue);
    }

}
