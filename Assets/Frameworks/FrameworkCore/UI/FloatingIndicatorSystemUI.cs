﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class FloatingIndicatorSystemUI : MonoBehaviour
{
    [SerializeField] RectTransform finishMovePoint;
    [SerializeField] float moveTime = 0.5f;
    [SerializeField] float scaleToIndicatorTime = 1;

    [SerializeField] GameObject indicatorPrefab;
    [SerializeField] string indicatorPoolKey;
    [SerializeField] int indicatorSpawnCount = 15;

    private void Start()
    {
        PoolManager.instance.CreatePool(indicatorPoolKey,indicatorPrefab, indicatorSpawnCount);
    }

    public void SpawnRandomMultiply(Vector3 pos,int _count)
    {
        for(int i = 0; i < _count; i++)
        {
            pos = Random.insideUnitCircle * 1;
            Spawn(pos);
        }
    }

    public void Spawn(Vector3 pos)
    {
        GameObject indi = PoolManager.instance.GetObject(indicatorPoolKey, pos, Quaternion.identity);
        indi.transform.parent = transform;

        indi.transform.DOMove(finishMovePoint.position, 1).OnComplete(() =>
        {
            PoolManager.instance.ReturnObjectToQueue(indi);
        });
    }


}
