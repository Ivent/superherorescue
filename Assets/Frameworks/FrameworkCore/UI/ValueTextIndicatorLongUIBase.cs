﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using UnityEngine.UI;
using Framework.Utility;

public class ValueTextIndicatorLongUIBase : MonoBehaviour
{
    ValueModelBaseLong valueModel;

    [SerializeField]
    Text valueTxt;

    public virtual void Init(ValueModelBaseLong _valueModel )
    {
        valueModel = _valueModel;

        valueModel.OnValueChanged += ValueModel_OnValueChanged;
        UpdateDetalText(); 
    }

    void ValueModel_OnValueChanged()
    {
        UpdateDetalText();
    }

    private void UpdateDetalText()
    {
        valueTxt.text =  Converter.GetKNotation( valueModel.CurrentValue);
    }

    private void OnDestroy()
    {
        valueModel.OnValueChanged -= ValueModel_OnValueChanged;
    }
}
