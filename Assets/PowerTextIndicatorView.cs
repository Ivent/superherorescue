﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerTextIndicatorView : MonoBehaviour
{
    [SerializeField] Power power;
    [SerializeField] Text powerText;

    [SerializeField] string textPrefix = "x";

    void Awake()
    {
        power.PowerValueChangeAction += PowerValueChangeAction;
    }

    void PowerValueChangeAction(int _power)
    {
        powerText.text = textPrefix+_power.ToString();
    }

    private void OnDestroy()
    {
        power.PowerValueChangeAction -= PowerValueChangeAction;
    }
}
