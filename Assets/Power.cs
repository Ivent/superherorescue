﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Power : MonoBehaviour
{

    [SerializeField] int defPower = 1;

    int power;

    public int GetPower => (int)power ;

    float targetPower;
    float changePowerTime;
    float lerpValue = 0;
    int prevPower;

    public Action<int> PowerValueChangeAction;

    private void Start()
    {
        AddPower(defPower,0);
        PowerValueChanged();
    }

    public void AddPower(int _count, float _time)
    {
        targetPower =  power + _count;
        changePowerTime = _time;
        lerpValue = 0;
    }

    private void Update()
    {
        lerpValue += Time.deltaTime / changePowerTime;
        prevPower = power;
        power = (int)Mathf.Lerp(power, targetPower, lerpValue);

        if (prevPower != power)
        {
            PowerValueChanged();
        }
    }

    void PowerValueChanged()
    {
        PowerValueChangeAction?.Invoke(power);
    }
}
