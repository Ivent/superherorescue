﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using Framework.Interfaces;
using Framework.Managers.Ads;
using UnityEngine;

public class SkipController : MonoBehaviour, IRewardedVideoAdListener
{
    [Inject] BaseAdSystem baseAdSystem;
    [Inject] GameLevelController gameLevelController;

    [SerializeField] SkipButtonView skipButtonView;

    void Start()
    {
        this.Inject();

        skipButtonView.BtnClickedAction += SkipButtonView_BtnClickedAction;
    }

    void SkipButtonView_BtnClickedAction()
    {
        baseAdSystem.SetRewardedVideoCallbacks(this);
        baseAdSystem.ShowRewardedVideo((bool obj) => { });
    }

    public void OnRewardedVideoLoaded() { }

    public void OnRewardedVideoShown() { }

    public void OnRewardedVideoFailedToLoad() { }

    public void OnRewardedVideoClosed(bool finished)
    {
        if (finished)
        {
            LoadNextLevel();
            return;
        }
    }

    public void OnRewardedVideoFinished(double amount, string name)
    {
        LoadNextLevel();
    }

    void LoadNextLevel()
    {
        gameLevelController.SetCurrentLevel(gameLevelController.CurrentLevel + 1);
        gameLevelController.Restart();
    }

    void OnDestroy()
    {
        skipButtonView.BtnClickedAction -= SkipButtonView_BtnClickedAction;
    }
}
