﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCanvasToCamera : MonoBehaviour
{
    [SerializeField] private GameObject _canvas;

    private Quaternion _rotationToCamera;

    private void Start()
    {
        _rotationToCamera = Camera.main.transform.rotation;
    }

    void Update()
    {
        if (_canvas.transform.rotation != _rotationToCamera)
        {
            _canvas.transform.rotation = _rotationToCamera;
        }
    }
}
