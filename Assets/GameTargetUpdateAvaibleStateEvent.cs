﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTargetUpdateAvaibleStateEvent : MonoBehaviour
{
    public Action TargetAvaibleStateUpdateAction;
}
