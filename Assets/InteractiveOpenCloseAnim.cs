﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class InteractiveOpenCloseAnim : MonoBehaviour
{
    public Transform OpenPos;
    public Transform ClosePos;

    public float closeTime;

    [SerializeField] Transform targetTransform;

    [SerializeField] InteractiveButton interactiveButton;

    void Start()
    {
        targetTransform.position = OpenPos.position;

        interactiveButton.OnInteract += InteractiveButton_OnInteract;
    }

    void InteractiveButton_OnInteract()
    {
        OnInteract();
        interactiveButton.OnInteract -= InteractiveButton_OnInteract;
    }


    void OnInteract()
    {
        targetTransform.DOMove(ClosePos.position, closeTime);
    }


}
