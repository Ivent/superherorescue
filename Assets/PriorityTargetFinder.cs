﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Adic;
using UnityEngine;
using UnityEngine.AI;

[System.Serializable]
public class TargetInfo
{
    public TargetTypeEnum targetType;
    public int priority = 0;
}

[System.Serializable]
public class AvaibleTargetData
{
    public Transform charTransform;
    public TargetInfo targetInfo;

    public AvaibleTargetData(Transform charTransform, TargetInfo targetInfo)
    {
        this.charTransform = charTransform;
        this.targetInfo = targetInfo;
    }
}

public class PriorityTargetFinder : MonoBehaviour
{
    [SerializeField] NavMeshAgent navMeshAgent;

     NavMeshPath navMeshPath;

    [SerializeField]
    List<TargetInfo> targetFinderParamList;

    
    List<AvaibleTargetData> avaibleTargetDatas = new List<AvaibleTargetData>();

    [Inject] TargetsOnLevel targetsOnLevel;
    [Inject] GameTargetUpdateAvaibleStateEvent gameTargetUpdateAvaibleStateEvent;


    void Start()
    {
        this.Inject();

        navMeshPath = new NavMeshPath();

        targetsOnLevel.TargetOnLevelsLoadEnd += TargetOnLevelsLoadEnd;

        gameTargetUpdateAvaibleStateEvent.TargetAvaibleStateUpdateAction += TargetAvaibleStateUpdateAction;


    }

    void TargetAvaibleStateUpdateAction()
    {
        BuildAvaibleTargetList();
    }

    void TargetOnLevelsLoadEnd()
    {
        BuildAvaibleTargetList();
    }

    void BuildAvaibleTargetList()
    {
        avaibleTargetDatas.Clear();
        for (int i = 0; i < targetsOnLevel.TargetDatas.Count; i++)
        {
            if (targetsOnLevel.TargetDatas[i].IsAvaible == true)
            {
                for (int k = 0; k < targetFinderParamList.Count; k++)
                {
                    if (targetsOnLevel.TargetDatas[i].targetTypeEnum == targetFinderParamList[k].targetType)
                    {
                        avaibleTargetDatas.Add(new AvaibleTargetData(targetsOnLevel.TargetDatas[i].transform, targetFinderParamList[k]));
                    }
                }
            }
        }

        avaibleTargetDatas = avaibleTargetDatas.OrderBy(x => x.targetInfo.priority).ToList();
    }

    public AvaibleTargetData GetTarget()
    {
        float minPathLength = 99999;
        float pathLength = 0;
        int minPathIndex = 0;

        int minPriorityValue = 999; ;

        List<AvaibleTargetData> avaibleToMoveTargets = new List<AvaibleTargetData>();

        for (int i = 0; i < avaibleTargetDatas.Count; i++)
        {
            NavMesh.SamplePosition(transform.position, out NavMeshHit hitA, 10f, NavMesh.AllAreas);
            NavMesh.SamplePosition(avaibleTargetDatas[i].charTransform.position, out NavMeshHit hitB, 10f, NavMesh.AllAreas);

            NavMeshPath path = new NavMeshPath();
            if (NavMesh.CalculatePath(hitA.position, hitB.position, NavMesh.AllAreas, path))
            {
                if(path.status == NavMeshPathStatus.PathComplete)
                {
                    avaibleToMoveTargets.Add(avaibleTargetDatas[i]);
                }
            }
        }

        if (avaibleToMoveTargets.Count == 0) return null;

        minPriorityValue = avaibleToMoveTargets.Min(x => x.targetInfo.priority);
        for (int i = 0; i < avaibleToMoveTargets.Count; i++)
        {
            if(avaibleToMoveTargets[i].targetInfo.priority == minPriorityValue)
            {
                pathLength = GetPathLength(navMeshPath);
                if (pathLength < minPathLength)
                {
                    minPathLength = pathLength;
                    minPathIndex = i;
                }
            }
        }
        return avaibleToMoveTargets[minPathIndex];
    }

    public  float GetPathLength(NavMeshPath path)
    {
        float lng = 0.0f;

        if ((path.status != NavMeshPathStatus.PathInvalid) && (path.corners.Length > 1))
        {
            for (int i = 1; i < path.corners.Length; ++i)
            {
                lng += Vector3.Distance(path.corners[i - 1], path.corners[i]);
            }
        }

        return lng;
    }

    private void OnDestroy()
    {
        targetsOnLevel.TargetOnLevelsLoadEnd -= TargetOnLevelsLoadEnd;

        gameTargetUpdateAvaibleStateEvent.TargetAvaibleStateUpdateAction -= TargetAvaibleStateUpdateAction;
    }
}
