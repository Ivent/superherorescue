﻿using System;
using Adic;
using UnityEngine;

public enum TargetTypeEnum
{
    HERO_FRIEND = 0,
    SUPERPOWER = 1,
    HERO_ENEMY = 2,
    LEVEL_BOSS = 3
}

public class TargetData : MonoBehaviour
{
    public TargetTypeEnum targetTypeEnum;
    public bool IsAvaible => isAvaible;

    [Inject] GameTargetUpdateAvaibleStateEvent gameTargetUpdateAvaibleStateEvent;

     bool isAvaible = true;

    private void Start()
    {
        this.Inject();
    }

    public void SetNoAvaibleState()
    {
        isAvaible = false;

        gameTargetUpdateAvaibleStateEvent.TargetAvaibleStateUpdateAction?.Invoke();

    }
}
