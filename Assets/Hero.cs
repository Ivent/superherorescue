﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero : MonoBehaviour
{
    [SerializeField] PriorityTargetFinder priorityTargetFinder;
    [SerializeField] MoveToTarget moveToTarget;
    [SerializeField] Power power;

    [SerializeField] Merge merge;

    AvaibleTargetData avaibleTargetData;

    void Start()
    {
        moveToTarget.StartMove();

        moveToTarget.MoveEndAction += MoveEndAction;

        Invoke("MoveToTarget", 1);
    }

    void MoveEndAction()
    {
        Interaction();
      
    }

    void Interaction()
    {
        if (isMerge())
        {
            moveToTarget.DisableMoveComponent();

            float mergeTime = 1;
            float powerAddTime = 1;

            merge.DoMerge(transform, avaibleTargetData.charTransform, mergeTime, () =>
            {
                int addPower = avaibleTargetData.charTransform.GetComponent<Power>().GetPower;

                if(avaibleTargetData.targetInfo.targetType == TargetTypeEnum.SUPERPOWER)
                {
                    addPower = (addPower * power.GetPower) -power.GetPower;
                }

                power.AddPower(addPower, powerAddTime);

                avaibleTargetData.charTransform.GetComponent<TargetData>().SetNoAvaibleState();

                avaibleTargetData = null;

                moveToTarget.EnableMoveComponent();


                Invoke("MoveToTarget", 1);
            });

           
        }
    }

    bool isMerge()
    {
        if (avaibleTargetData == null) return false;
        return (avaibleTargetData.targetInfo.targetType == TargetTypeEnum.HERO_FRIEND || avaibleTargetData.targetInfo.targetType == TargetTypeEnum.SUPERPOWER);
    }

    void MoveToTarget()
    {
        moveToTarget.StartMove();

        avaibleTargetData = priorityTargetFinder.GetTarget();

        if(avaibleTargetData != null)

        moveToTarget.SetNewTarget(avaibleTargetData.charTransform, 0.4f);
    }

    private void OnDestroy()
    {
        moveToTarget.MoveEndAction -= MoveEndAction;
    }
}
