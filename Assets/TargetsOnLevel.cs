﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;

public class TargetsOnLevel : MonoBehaviour
{
    [Inject] GameLevelController gameLevelController;

    public List<TargetData> TargetDatas => targetDatas;
    List<TargetData> targetDatas = new List<TargetData>();


    public Action TargetOnLevelsLoadEnd;

    void Start()
    {
        this.Inject();
        gameLevelController.OnLoadFinished += OnLoadFinished;
    }

    void OnLoadFinished()
    {
        targetDatas.Clear();
        targetDatas.AddRange(FindObjectsOfType<TargetData>());
        TargetOnLevelsLoadEnd?.Invoke();
    }


    private void OnDestroy()
    {
        gameLevelController.OnLoadFinished -= OnLoadFinished;

    }
}
