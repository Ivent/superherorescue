﻿using System;
using UnityEngine;
using UnityEngine.AI;

public class MoveToTarget : MonoBehaviour
{
    public NavMeshAgent navMeshAgent;
    public Transform target;

    public bool IsMoving => isMoving;
    bool isMoving;

    [SerializeField] float dest = 1f;

    public Action MoveEndAction;

    public void DisableMoveComponent()
    {
        navMeshAgent.enabled = false;
    }

    public void EnableMoveComponent()
    {
        navMeshAgent.enabled = true;
    }

    public void StartMove()
    {
        isMoving = true;
    }

    public void StopMove()
    {
        isMoving = false;

        MoveEndAction?.Invoke();
    }

    public void SetNewTarget(Transform _target, float _stopDistance )
    {
        target = _target;
        navMeshAgent.stoppingDistance = _stopDistance;
    }

    void ShowDebugPath()
    {
        for (int i = 0; i < navMeshAgent.path.corners.Length - 1; i++)
            Debug.DrawLine(navMeshAgent.path.corners[i], navMeshAgent.path.corners[i + 1], Color.red);
    }



    void CheckDestinationReached()
    {
        float distanceToTarget = Vector3.Distance(transform.position, target.position);
        if (distanceToTarget < dest)
        {
            StopMove();
        }
    }

    void Update()
    {
        if (isMoving && target != null)
        {
            navMeshAgent.SetDestination(target.transform.position);
            //ShowDebugPath();
            CheckDestinationReached();
        }
    }
}
