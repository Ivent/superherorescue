﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraGuide : MonoBehaviour
{
    [SerializeField] float maxY = 100;

    [SerializeField] float offset = 5;

    [SerializeField] float maxCurrentYValue;
 

    public void ChangeY(float _newY)
    {
        Vector3 newPos = new Vector3(transform.position.x, _newY, transform.position.z);
        transform.position = newPos;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer == 9)
        {
            Vector3 newPos = new Vector3(transform.position.x,collision.transform.position.y + offset, transform.position.z);

            if (newPos.y > maxCurrentYValue)
            {
                transform.position = newPos;
                maxCurrentYValue = newPos.y;

                if(newPos.y > maxY )
                {
                    gameObject.SetActive(false);
                }
            }
        }
    }
}
