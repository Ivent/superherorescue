﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Merge : MonoBehaviour
{
  
    public void DoMerge(Transform originObj, Transform targetObj, float _mergeTime, Action OnComplete)
    {
        Vector3 centerPos = Vector3.Lerp(originObj.position, targetObj.position, 0.5f);
        targetObj.DOMove(centerPos, _mergeTime);

        originObj.DOMove(centerPos, _mergeTime).OnComplete(()=>
        {
            targetObj.gameObject.SetActive(false);
            OnComplete?.Invoke();
            originObj.DOPunchScale(new Vector3(0.2f, 0.2f, 0.2f), _mergeTime * 0.3f);


        });
    }
}
