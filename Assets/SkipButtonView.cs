﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkipButtonView : MonoBehaviour
{
    public Action BtnClickedAction;

    [SerializeField] Button skipBtn;

    void Start()
    {
        skipBtn.onClick.AddListener(SkipBtnClickHandler);
    }

    void SkipBtnClickHandler()
    {
        BtnClickedAction?.Invoke();
    }

    public void ActivateButton()
    { 
       skipBtn.interactable = true;
    }

    public void DeactivateButton()
    {
        skipBtn.interactable = false;
    }

    private void OnDestroy()
    {
        skipBtn.onClick.RemoveListener(SkipBtnClickHandler);
    }
}
