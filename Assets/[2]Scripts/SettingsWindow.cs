﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsWindow : MonoBehaviour
{
    [SerializeField] GameObject visual;


    [SerializeField] ButtonSwitcherView soundBtnView;
    [SerializeField] ButtonSwitcherView vibroBtnView;

    [SerializeField] Button closeBtn;

   // [Inject] UpdateManager updateManager;
   // [Inject] VibroSwitcherController vibrationSwitcherController;
   // [Inject] SoundSwitcherController soundSwitcherController;



    void Start()
    {
        this.Inject();


        closeBtn.onClick.AddListener(CloseBtnHandler);
    }



   public void Show()
    {
        //soundSwitcherController.Load(soundBtnView, updateManager);
        // VibrationSwitcherController.Load(vibroBtnView, updateManager);
        visual.SetActive(true);
    }

    void Hide()
    {
       // soundSwitcherController.Unload();
        //   VibrationSwitcherController.Unload();
        visual.SetActive(false);
    }

    void CloseBtnHandler()
    {
        Hide();
    }

    private void OnDestroy()
    {
       // soundSwitcherController.Unload();
        //   VibrationSwitcherController.Unload();
        closeBtn.onClick.RemoveListener(CloseBtnHandler);
    }

}
