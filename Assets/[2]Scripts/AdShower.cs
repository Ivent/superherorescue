﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using Framework.Managers.Ads;
using UnityEngine;

public class AdShower : MonoBehaviour
{
    [Inject] BaseAdSystem baseAdSystem;
    [Inject] GameLevelController gameLevelController;

    [Inject] GameWinController gameWinController;
    [Inject] GameLooseController gameLooseController;


    int adShowPeriod;
    int defAdShowPeriod = 2;

    void Start()
    {
        this.Inject();

        baseAdSystem.Init();

        adShowPeriod = defAdShowPeriod;

        gameLevelController.OnLoadFinished += GameLevelController_OnLoadFinished;

        gameWinController.OnGameWin += GameWinController_OnGameWin;
        gameLooseController.OnLoose += GameLooseController_OnLoose;
    }

    void GameLooseController_OnLoose()
    {
        baseAdSystem.ShowBanner();
    }


    void GameWinController_OnGameWin()
    {
        baseAdSystem.ShowBanner();
    }


    void GameLevelController_OnLoadFinished()
    {
        baseAdSystem.HideBanner();
        
        adShowPeriod--;

        if(adShowPeriod <= 0)
        {
            adShowPeriod = defAdShowPeriod;

            if (gameLevelController.CurrentLevel > 5)
            {
                defAdShowPeriod = 1;
            }

            baseAdSystem.ShowInterstetial((bool obj) => { });
        }
    }

    private void OnDestroy()
    {
        gameLevelController.OnLoadFinished -= GameLevelController_OnLoadFinished;
    }
}
