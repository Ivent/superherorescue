﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using UnityEngine.UI;

public class StarViewUI : MonoBehaviour
{
    [SerializeField]
    Image starImg1;
    [SerializeField]
    Image starImg2;
    [SerializeField]
    Image starImg3;

    [Inject] GameLevelController gameLevelController;
    [Inject] IStarCounter starCounter;

    [SerializeField] bool isInverse;


    void Start()
    {
        this.Inject();

        HideAllStars();
        UpdateStarView();

        starCounter.OnStarChanged += StarCounter_OnStarChanged;

        gameLevelController.OnLoadFinished += GameLevelController_OnLoadFinished;
    }

    public void SetNewCounter(IStarCounter _starCounter)
    {
        starCounter.OnStarChanged -= StarCounter_OnStarChanged;

        starCounter = _starCounter;

        HideAllStars();
        UpdateStarView();
        starCounter.OnStarChanged += StarCounter_OnStarChanged;
    }

    void GameLevelController_OnLoadFinished()
    {
        HideAllStars();
        UpdateStarView();
    }


    void HideAllStars()
    {
        starImg1.fillAmount = 0;
        starImg2.fillAmount = 0;
        starImg3.fillAmount = 0;
    }

    void UseStar(Image _starImg)
    {
        _starImg.fillAmount = 1;
    }

    private void Update()
    {
        starImg1.fillAmount = starCounter.Get1StarProgress();
        starImg2.fillAmount = starCounter.Get2StarProgress();
        starImg3.fillAmount = starCounter.Get3StarProgress();
    }

    void StarCounter_OnStarChanged(int _currentStars)
    {
        UpdateStarView();
    }

    public void UpdateStarView()
    {
        if (starCounter.CurrentStars >= 1)
        {
            UseStar(starImg1);
        }

        if (starCounter.CurrentStars >= 2)
        {
            UseStar(starImg2);
        }

        if (starCounter.CurrentStars == 3)
        {
            UseStar(starImg3);
        }
    }

    private void OnDestroy()
    {
        gameLevelController.OnLoadFinished -= GameLevelController_OnLoadFinished;
        starCounter.OnStarChanged -= StarCounter_OnStarChanged;
    }
}
