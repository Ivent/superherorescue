﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using UnityEngine.UI;

public class GamePanelView : MonoBehaviour
{
    [Inject] GameLevelController gameLevelController;
    [Inject] OilCounter oilCounter;
    [Inject] StarCounter starCounter;
    [Inject] GameWinController gameWinController;
    [Inject] SettingsWindow settingsWindow;

    [SerializeField] ProgressBarWithTextFillingValue progressBarWithTextFillingValue;

    [SerializeField] Button restartBtn;
    [SerializeField] Button settingBtn;
    [SerializeField] Button skipBtn;
    [SerializeField] Button winBtn;

    public Action SkipBtnClickAction;

    void Start()
    {
        this.Inject();

        oilCounter.OnOilCountChanged += OilCounter_OnOilCountChanged;
        gameLevelController.OnLoadFinished += GameLevelController_OnLoadFinished;
        starCounter.OnStarChanged += StarCounter_OnStarChanged;

        winBtn.onClick.AddListener(WinBtnHandlerClicked);
        restartBtn.onClick.AddListener(RestarnBtnHandlerClicked);
        settingBtn.onClick.AddListener(SettingsBtnClickHandler);

        skipBtn.onClick.AddListener(SkipBtnClickHandler);
    }

    void GameLevelController_OnLoadFinished()
    {
        HideWinBtn();
    }

    void SkipBtnClickHandler()
    {
        SkipBtnClickAction?.Invoke();
    }

    void SettingsBtnClickHandler()
    {
        settingsWindow.Show();
    }

    void StarCounter_OnStarChanged(int _stars)
    {
        if(_stars == 1)
        {
            ShowWinBtn();
        }
    }

    void OilCounter_OnOilCountChanged()
    {
        progressBarWithTextFillingValue.FillValue(oilCounter.CurrentOilCount, oilCounter.TotalOilCount);
    }

    void ShowWinBtn()
    {
        winBtn.gameObject.SetActive(true);
    }

    void HideWinBtn()
    {
        winBtn.gameObject.SetActive(false);
    }

    void WinBtnHandlerClicked()
    {
        gameWinController.GameWin();
    }

    void RestarnBtnHandlerClicked()
    {
        gameLevelController.Restart();
    }

    private void OnDestroy()
    {
        oilCounter.OnOilCountChanged -= OilCounter_OnOilCountChanged;
        gameLevelController.OnLoadFinished -= GameLevelController_OnLoadFinished;
        starCounter.OnStarChanged -= StarCounter_OnStarChanged;

        restartBtn.onClick.RemoveListener(RestarnBtnHandlerClicked);
        winBtn.onClick.RemoveListener(WinBtnHandlerClicked);
        settingBtn.onClick.RemoveListener(SettingsBtnClickHandler);
        skipBtn.onClick.RemoveListener(SkipBtnClickHandler);
    }

}
