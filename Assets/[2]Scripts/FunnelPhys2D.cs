﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;

public class FunnelPhys2D : MonoBehaviour
{

    public Action OnWaterEntered =  delegate {};

    [Inject] OilCounter oilCounter;
    [Inject] GameWinController gameWinController;

    bool isOn;

    private void Start()
    {
        this.Inject();
        TurnOn();
        gameWinController.OnGameWin += GameWinController_OnGameWin;

    }

    void GameWinController_OnGameWin()
    {
        TurnOff();
    }

    public void TurnOn()
    {
        isOn = true;
    }

    public void TurnOff()
    {
        isOn = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isOn)
        {
            StartCoroutine(ExampleCoroutine(collision));
        }
        else
        {
            collision.gameObject.SetActive(false);
        }

    }

    IEnumerator ExampleCoroutine(Collider2D collision)
    {
        Rigidbody2D rb = collision.GetComponent<Rigidbody2D>();
        rb.drag = 0;
        rb.gravityScale = 0f;
        rb.angularDrag = 0;
        rb.isKinematic = true;
        rb.MovePosition(transform.position);
        yield return new WaitForSeconds(0.1f);

        collision.gameObject.SetActive(false);
        oilCounter.DecOil();
        OnWaterEntered.Invoke();
    }


    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (isOn)
    //    {
    //        collision.gameObject.SetActive(false);
    //        oilCounter.DecOil();
    //        OnWaterEntered.Invoke();
    //    }
    //}

    private void OnDestroy()
    {
        gameWinController.OnGameWin -= GameWinController_OnGameWin;
    }
}
