﻿using System;
using Adic;
using Framework;
using UnityEngine;

public class GameLevelController : MonoBehaviour,ILoader
{

    [Inject] LevelPrefabLoader levelLoader;

    [SerializeField]
    int maxLevel = 1;

    int maxCompletedLevel;

    int currentLevel;
    public int CurrentLevel { get => currentLevel; set => currentLevel = value; }
    public int MaxLevel { get => maxLevel; }
    public int MaxCompletedLevel { get => maxCompletedLevel;  }

    public Action OnLoadFinished { get => onLevelLoadFinished; set => onLevelLoadFinished = value; }
    public Action OnLoadStarted { get => onLevelLoadStarted; set => onLevelLoadStarted = value; }

     Action onLevelLoadFinished;
    Action onLevelLoadStarted;

    float loadingDelay = 0.5f;

    void Awake()
    {
        currentLevel = PlayerPrefs.GetInt("currentLevel", 1);
        maxCompletedLevel = PlayerPrefs.GetInt("maxCompletedLevel");
    }

    void Start()
    {
        this.Inject();

        LoadCurrentLevel();
        levelLoader.OnLoadFinished += LevelLoader_OnMapBuilded;
    }

    private void LoadCurrentLevel()
    {
        onLevelLoadStarted?.Invoke();
        Invoke("StartLoad", loadingDelay);
     
    }

    public bool MaxLevelIsCompleted()
    {
        return maxCompletedLevel == maxLevel;
    }

    void StartLoad()
    {
        levelLoader.LoadLevel( new LevelLoaderPatchProvider(currentLevel));
    }

    void LevelLoader_OnMapBuilded()
    {
        onLevelLoadFinished?.Invoke();
    }

    public void SetCurrentLevel( int _currentLevel)
    {
        if (_currentLevel > maxCompletedLevel)
        {
            maxCompletedLevel = currentLevel;
            PlayerPrefs.SetInt("maxCompletedLevel", maxCompletedLevel);
        }

        if (_currentLevel <= MaxLevel && _currentLevel > 0)
        {
            currentLevel = _currentLevel;
            PlayerPrefs.SetInt("currentLevel", _currentLevel);
        } 
    }


    public void LoadNextLevel()
    {
        //statisticSystemController.SendLevelWinEvent(currentLevel);
        LoadCurrentLevel();
    }

    public void LoadPrevLevel()
    {
        SetCurrentLevel(CurrentLevel - 1);
        LoadCurrentLevel();
    }

    public void Restart()
    {
        //playerStatistics.Restars++;
        // if (playerStatistics.Restars % 2 == 0)
        // {
        //baseAdSystem.ShowInterstetial((bool isReady) =>
        //{
        //    if(isReady == false)
        //    {
        //        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        //        return;
        //    }
        //});
        // }
        //SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        LoadCurrentLevel();
    }

    private void OnDestroy()
    {
        levelLoader.OnLoadFinished -= LevelLoader_OnMapBuilded;
    }
}
