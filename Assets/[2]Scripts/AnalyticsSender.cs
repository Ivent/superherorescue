﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using Framework.Managers.Statistics;
using UnityEngine;

public class AnalyticsSender : MonoBehaviour
{
    [Inject] StatisticSystemController statisticSystem;


    [Inject] GameWinController gameWinController;
    [Inject] GameLooseController gameLooseController;
    [Inject] GameLevelController gameLevelController;

    void Start()
    {
        this.Inject();

//        statisticSystem.AddStatisticSystem( new SS_AppsFlyerAnalytics());
  //      statisticSystem.AddStatisticSystem( new SS_GameAnalytics());
        //statisticSystem.AddStatisticSystem(new SS_FacebookAnalytics());
        statisticSystem.Init();

        gameWinController.OnGameWin += GameWinController_OnGameWin;
        gameLooseController.OnLoose += GameLooseController_OnLoose;

        gameLevelController.OnLoadFinished += LevelLoadFinished;

        Debug.Log("ANALYTICS INIT");
    }

    void LevelLoadFinished()
    {
        Debug.Log("LVL STARTED");
    }

    void GameLooseController_OnLoose()
    {
        Debug.Log("LOOSE");


        statisticSystem.SendLevelWinEvent(gameLevelController.CurrentLevel);
    }


    void GameWinController_OnGameWin()
    {
        Debug.Log("WING");

        statisticSystem.SendLevelWinEvent(gameLevelController.CurrentLevel);
    }

    private void OnDestroy()
    {
        gameWinController.OnGameWin -= GameWinController_OnGameWin;
        gameLooseController.OnLoose -= GameLooseController_OnLoose;
        gameLevelController.OnLoadFinished -= LevelLoadFinished;

    }
}
