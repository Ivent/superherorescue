﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using UnityEngine.UI;

public class GameLooseView : MonoBehaviour
{
    [Inject] GameLevelController gameLevelController;
    [Inject] GameLooseController gameLooseController;
    [Inject] MenuView menuView;

    [SerializeField] GameObject visual;

    [SerializeField] Button restartBtn;
    [SerializeField] Button menuBtn;

    private void Start()
    {
        this.Inject();

        gameLooseController.OnLoose += GameLooseController_OnLoose;

        restartBtn.onClick.AddListener(RestarnBtnHandlerClicked);
        menuBtn.onClick.AddListener(MenuBtnHandlerClicked);
    }

    void GameLooseController_OnLoose()
    {
       // Show();
    }

    void Show()
    {
       
        visual.SetActive(true);
    }

    void MenuBtnHandlerClicked()
    {
        menuView.Show();
        Hide();
    }


    void RestarnBtnHandlerClicked()
    {
        gameLevelController.SetCurrentLevel(gameLevelController.CurrentLevel);
        gameLevelController.Restart();
        Hide();
    }

    void Hide()
    {
        visual.SetActive(false);
    }

    private void OnDestroy()
    {
        gameLooseController.OnLoose -= GameLooseController_OnLoose;
        restartBtn.onClick.RemoveListener(RestarnBtnHandlerClicked);
        menuBtn.onClick.RemoveListener(MenuBtnHandlerClicked);
    }
}
