﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using UnityEngine.UI;

public class TestPanelUI : MonoBehaviour
{
    Button loadNextLevel;
    Button loadPrevLevel;

    [Inject] GameLevelController gameLevelController;

    private void Start()
    {
        this.Inject();
    }

    public void LoadPrevLevelBtnClicked()
    {
        gameLevelController.LoadPrevLevel();
    }

    public void LoadNextLevelBtnClicked()
    {
        gameLevelController.SetCurrentLevel(gameLevelController.CurrentLevel + 1);
        gameLevelController.LoadNextLevel();
    }
}
