﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using UnityEngine.UI;

public class GameWinView : MonoBehaviour
{
    [Inject] GameWinController gameWinController;
    [Inject] GameLevelController gameLevelController;

    [Inject] MenuView menuView;

    [SerializeField] Button restartBtn;
    [SerializeField] Button nextBtn;
    [SerializeField] Button menuBtn;

    [SerializeField] Text levelTxt;

    [SerializeField] GameObject visual;

   // [SerializeField] StarViewUI starViewUI;

    void Start()
    {
        this.Inject();

        gameWinController.OnGameWin += GameWinController_OnGameWin;

        restartBtn.onClick.AddListener(RestarnBtnHandlerClicked);
        nextBtn.onClick.AddListener(NextLevelBtnHandlerClicked);
        menuBtn.onClick.AddListener(MenuBtnHandlerClicked);



    }

    void MenuBtnHandlerClicked()
    {
        menuView.Show();
        Hide();
    }

    void NextLevelBtnHandlerClicked()
    {
        gameLevelController.LoadNextLevel();
        Hide();
    }

    void RestarnBtnHandlerClicked()
    {
        gameLevelController.SetCurrentLevel(gameLevelController.CurrentLevel-1);
        gameLevelController.Restart();
        Hide();
    }

    void GameWinController_OnGameWin()
    {
      //  Show();
    }

    void Show()
    {
        visual.SetActive(true);
        levelTxt.text = "LEVEL " + gameLevelController.CurrentLevel.ToString();

        gameLevelController.SetCurrentLevel(gameLevelController.CurrentLevel + 1);

       // starViewUI.UpdateStarView();
    }

    void Hide()
    {
        visual.SetActive(false);
    }

    private void OnDestroy()
    {
        restartBtn.onClick.RemoveListener(RestarnBtnHandlerClicked);
        nextBtn.onClick.RemoveListener(NextLevelBtnHandlerClicked);
        menuBtn.onClick.RemoveListener(MenuBtnHandlerClicked);

        gameWinController.OnGameWin -= GameWinController_OnGameWin;
    }
}
