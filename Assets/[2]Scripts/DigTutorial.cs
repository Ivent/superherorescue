﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DigTutorial : MonoBehaviour
{
    [SerializeField]
    GameObject digHint;

    Vector3 fingerStartPos;
    Vector3 fingetCurrentPos;

    int isActive;

    void Start()
    {
        isActive = PlayerPrefs.GetInt("DigTutor");

        HideHint();

        if (isActive == 0)
        {
            StartTutor();
        }
    }

    void StartTutor()
    {
        ShowHint();
    }

    void EndTutor()
    {
        isActive = 1;
        HideHint();
        PlayerPrefs.SetInt("DigTutor", isActive);
    }

    void ShowHint()
    {
        digHint.SetActive(true);
    }

    void HideHint()
    {
        digHint.SetActive(false);
    }

    void Update()
    {
        if (isActive == 1) return;

        if(Input.GetMouseButton(0))
        {
            if(fingerStartPos == Vector3.zero)
            {
                fingerStartPos = Input.mousePosition;
            }
            else
            {
                if(  (Input.mousePosition-fingerStartPos).magnitude > 60)
                {
                    EndTutor();
                }
            }

        }
    }
}
