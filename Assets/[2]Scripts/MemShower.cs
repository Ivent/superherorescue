﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework.Utility;
using DG.Tweening;

public class MemShower : MonoBehaviour
{

    [SerializeField] Transform startPos;
    [SerializeField] Transform endPos;

    [SerializeField]
    List<RectTransform> mems = new List<RectTransform>();


    private void OnEnable()
    {
        RectTransform randomMem = GetRandoMemAndHideOthers();
        randomMem.gameObject.SetActive(true);
        ShowMem(randomMem);

    }

    RectTransform GetRandoMemAndHideOthers()
    {
        int randomMemIndex = Random.Range(0, mems.Count);

        for (int i = 0; i < mems.Count; ++i)
        {
            if(i != randomMemIndex)
            {
                mems[i].gameObject.SetActive(false);
            }
        }

        return mems[randomMemIndex];
    }

    void ShowMem(RectTransform mem)
    {
        mem.position = startPos.position;
        mem.DOMove(endPos.position, 2).SetEase(Ease.OutFlash);
    }

}
