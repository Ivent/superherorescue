﻿using System.Collections;
using System.Collections.Generic;
using Framework.Components;
using UnityEngine;

[RequireComponent(typeof(Explosion), typeof(CircleClipper), typeof(ExplosiveActivator))]
public class ExplosiveDig : MonoBehaviour
{
    Explosion explosion;
    ExplosiveActivator explosiveActivator;
    CircleClipper circleClipper;

    Vector3 boomPos;

    bool isUsed;

    void Start()
    {
        explosion = GetComponent<Explosion>();
        circleClipper = GetComponent<CircleClipper>();
        explosiveActivator = GetComponent<ExplosiveActivator>();

      //  destrictTerr = GameObject.FindGameObjectWithTag("DestrictionTag").GetComponent<Terrain2D>();
        explosiveActivator.OnPreBoom += ExplosiveActivator_OnPreBoom;
    }

    void ExplosiveActivator_OnPreBoom()
    {

        circleClipper.Clip();
      //  destrictTerr.dig(boomPos, explosion.Explosion_max_sixe*2, 1);
    }

    private void OnDestroy()
    {
        explosiveActivator.OnPreBoom -= ExplosiveActivator_OnPreBoom;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (isUsed) return;
        isUsed = false;

        boomPos = collision.transform.position;
    }
}
