﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ExplosiveActivator))]
public class ExplosiveTrigger : MonoBehaviour
{

    ExplosiveActivator explosiveActivator;

    bool isUsed;

    private void Start()
    {
        explosiveActivator = GetComponent<ExplosiveActivator>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (isUsed) return;

        if (collision.gameObject.tag == "Metaball_liquid" || collision.gameObject.tag == "DynamicObstacles")
        {
            explosiveActivator.Interact();
            isUsed = true;
        }
    }
}
