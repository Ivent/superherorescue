﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ExplosiveActivator))]
public class ExplosiveVisual : MonoBehaviour
{
    ExplosiveActivator explosiveActivator;

    [SerializeField] GameObject explosivePrefab;

    Animator animator;

    void Awake()
    {
        animator = GetComponent<Animator>();
        explosiveActivator = GetComponent<ExplosiveActivator>();

        PoolManager.instance.CreatePool("explosive1Pool", explosivePrefab, 5);

        explosiveActivator.OnBoom += ExplosiveActivator_OnBoom;
        explosiveActivator.OnActivatorUsed += ExplosiveActivator_OnActivatorUsed;
    }

    void ExplosiveActivator_OnActivatorUsed()
    {
        animator.SetTrigger("preboom");
        PoolManager.instance.GetObject("explosive1Pool", new Vector3(transform.position.x, transform.position.y, -63f), Quaternion.identity);
    }


    void ExplosiveActivator_OnBoom()
    {

    }

    private void OnDestroy()
    {
        explosiveActivator.OnBoom -= ExplosiveActivator_OnBoom;
        explosiveActivator.OnActivatorUsed -= ExplosiveActivator_OnActivatorUsed;
    }
}
