﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveActivator : MonoBehaviour,IInteractiveObject
{
    public Action OnActivatorUsed;
    public Action OnPreBoom;
    public Action OnBoom;

    [SerializeField]
    float delayToBoom = 0;

    bool isUsed;


    public void Interact()
    {
        if (isUsed == true)
            return;
        OnActivatorUsed?.Invoke();
        isUsed = true;
        Invoke("Boom", delayToBoom);
    }

    void Boom()
    {
        OnPreBoom?.Invoke();
        OnBoom?.Invoke();
    }

}
