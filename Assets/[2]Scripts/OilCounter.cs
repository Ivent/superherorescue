﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using Water2D;

public class OilCounter : MonoBehaviour
{
    [SerializeField]
    int totalOilCount;

    int currentOilCount;

    public Action OnOilCountChanged = delegate {};

    [Inject] GameLevelController gameLevelController;

    public int CurrentOilCount { get => currentOilCount; set => currentOilCount = value; }
    public int TotalOilCount { get => totalOilCount; set => totalOilCount = value; }

    Water2D_Spawner[] water2DSpawners;

    void Start()
    {
       
        this.Inject();

        gameLevelController.OnLoadFinished += GameLevelController_OnLoadFinished;

    }

    void GameLevelController_OnLoadFinished()
    {
        CountTotalOil();
        CurrentOilCount = TotalOilCount;
        OnOilCountChanged();
    }

    void CountTotalOil()
    {

        TotalOilCount = 0;

        water2DSpawners = FindObjectsOfType<Water2D_Spawner>();

        for (int i = 0; i < water2DSpawners.Length; i++)
        {
            TotalOilCount += water2DSpawners[i].AllBallsCount;
        }
    }

    public float GetFillRatio()
    {
        return (float)CurrentOilCount / TotalOilCount;
    }

    public void DecOil()
    {
        CurrentOilCount--;
        OnOilCountChanged();
    }

    public void AddOil()
    {
        CurrentOilCount++;
        OnOilCountChanged();
    }

    private void OnDestroy()
    {
        for (int i = 0; i < water2DSpawners.Length; i++)
        {
            TotalOilCount -= water2DSpawners[i].AllBallsCount;
        }

        gameLevelController.OnLoadFinished -= GameLevelController_OnLoadFinished;
    }

}
