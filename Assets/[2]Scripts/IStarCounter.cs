﻿using System;

public interface IStarCounter
{
    Action<int> OnStarChanged { get; set; }
    Action<int> OnStarAdded { get; set; }
    int CurrentStars { get; set; }
    int Stars { get; set; }
    float Get1StarProgress();
    float Get2StarProgress();
    float Get3StarProgress();
}