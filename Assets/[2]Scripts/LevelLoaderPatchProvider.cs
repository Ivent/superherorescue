﻿using System;

public class LevelLoaderPatchProvider:IPathProvider
{
    int levelNumber;
    public LevelLoaderPatchProvider(int _levelNumber)
    {
        levelNumber = _levelNumber;
    }

    public string GetPath()
    {
       return string.Format("Levels/Level{0}", levelNumber);
    }
}
