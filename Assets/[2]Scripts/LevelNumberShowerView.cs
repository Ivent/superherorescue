﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class LevelNumberShowerView : MonoBehaviour
{
    [Inject] GameLevelController gameLevelController;

    [SerializeField]
    GameObject visual;

    [SerializeField]
    float showTimeInSec;

    [SerializeField]
    Text levelTxt;

    void Start()
    {
        this.Inject();

        Hide();

        gameLevelController.OnLoadFinished += GameLevelController_OnLoadFinished;
    }

    void GameLevelController_OnLoadFinished()
    {
        Show();
    }

    void Show()
    {
        levelTxt.text = "LEVEL " + gameLevelController.CurrentLevel.ToString();
        visual.SetActive(true);
      //  Invoke("Hide", showTimeInSec);

    }

    void Hide()
    {
        visual.SetActive(false);
    }

    private void OnDestroy()
    {
        gameLevelController.OnLoadFinished -= GameLevelController_OnLoadFinished;
    }
}
