﻿using UnityEngine;
using System.Collections;
using Adic;

public class TotalStarsCounter : MonoBehaviour
{
    [Inject] IStarCounter starCounter;

    int totalStars;

    public int TotalStars { get => totalStars;  }

    void Start()
    {
        this.Inject();

        totalStars = PlayerPrefs.GetInt("TotalStars");

        starCounter.OnStarAdded += StarCounter_OnStarAdded;
    }

    void StarCounter_OnStarAdded(int newStar)
    {
        totalStars += newStar;
        PlayerPrefs.SetInt("TotalStars", totalStars);
    }

    private void OnDestroy()
    {
        starCounter.OnStarAdded -= StarCounter_OnStarAdded;
    }
}
