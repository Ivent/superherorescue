﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using UnityEngine.UI;

public class LevelMapView : MonoBehaviour
{
    [SerializeField] GameObject visual;
    [SerializeField] Button backBtn;

    public Action OnLevelMapBackBtnClicked;

    void Start()
    {
        this.Inject();
        backBtn.onClick.AddListener(BackBtnClicked);
    }

    void BackBtnClicked()
    {
        Hide();
        OnLevelMapBackBtnClicked?.Invoke();
    }


    public void Show()
    {
        visual.SetActive(true); 
    }

    public void Hide()
    {
        visual.SetActive(false);
    }

    private void OnDestroy()
    {
        backBtn.onClick.RemoveListener(BackBtnClicked);
    }
}
