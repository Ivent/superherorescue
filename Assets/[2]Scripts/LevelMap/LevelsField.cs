﻿using System;
using System.Collections;
using System.Collections.Generic;
using Framework.Interfaces;
using UnityEngine;
using UnityEngine.UI;

public class LevelsField : MonoBehaviour {

    [SerializeField] GameObject levelMapItemPrefab;

    [SerializeField] Transform levelMapItemContainer;
    [SerializeField] List<LevelMapItem> levelMapItems;

    int maxLevelIsCompleted;

    int maxLevel;
    int minLevel;
    int maxCompletedLevel;

    public void Load()
    {
        gameObject.SetActive(true);
    }

    public void Unload()
    {
        gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        SetupLevelMapItemsStates();
    }

    public void InitLevelMap(int _minLevel, int _maxLevel, int _maxCompletedLevel,int _maxLevelIsCompleted)
    {
        minLevel = _minLevel;
        maxLevel = _maxLevel;
        maxCompletedLevel = _maxCompletedLevel;

        maxLevelIsCompleted = _maxLevelIsCompleted;
        StartCoroutine(FillLevelMap());
    }

    IEnumerator FillLevelMap()
    {
        if(levelMapItems.Count == 0)
        {
            yield return StartCoroutine(BuildLevelMap());
        }
        SetupLevelMapItemsStates();
        yield return null;
    }

    void SetupLevelMapItemsStates()
    {
        for (int i = 0; i < levelMapItems.Count; i++)
        {
        
            if ((minLevel + i) <= maxCompletedLevel || maxLevelIsCompleted == 1)
            {
                levelMapItems[i].SetUnlockedState();
            }
            else
            {
                levelMapItems[i].SetLockedState();
            }

            if (maxLevelIsCompleted == 0)
            {
                if (levelMapItems[i].GetLevel() == maxCompletedLevel + 1)
                {
                    levelMapItems[i].SetUnlockedState();
                    levelMapItems[i].SetNewLevelState();
                }
            }
        }

    }

    IEnumerator BuildLevelMap()
    {
        for (int i = minLevel; i < maxLevel+1; i++)
        {

            LevelMapItem newLevelMapItem = Instantiate(levelMapItemPrefab, levelMapItemContainer).GetComponent<LevelMapItem>();
            levelMapItems.Add(newLevelMapItem);
            newLevelMapItem.SetLevel(i);
  
        }
        yield return null;
    }
}
