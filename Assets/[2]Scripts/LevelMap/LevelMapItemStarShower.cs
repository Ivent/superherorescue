﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LevelMapItem))]
public class LevelMapItemStarShower : MonoBehaviour,IStarCounter
{
    LevelMapItem levelMapItem;
    [SerializeField] StarViewUI starViewUI;

    int level;
    int stars;

    Action<int> onStarChanged;
    Action<int> onStarAdded;

    public Action<int> OnStarChanged { get => onStarChanged; set => onStarChanged = value; }
    public Action<int> OnStarAdded { get { return onStarAdded; } set { onStarAdded = value; } }

    public int CurrentStars { get => stars; set => stars = value; }
    public int Stars { get => stars; set => stars = value; }

    void Start()
    {
        levelMapItem =  GetComponent<LevelMapItem>();

        levelMapItem.UnlockAction += LevelMapItem_UnlockAction;
    }

    void LevelMapItem_UnlockAction()
    {
        level = levelMapItem.GetLevel();
        stars =  PlayerPrefs.GetInt("s" + level);
        starViewUI.SetNewCounter(this);
    }

    private void OnDestroy()
    {
        levelMapItem.UnlockAction -= LevelMapItem_UnlockAction;
    }

    public float Get1StarProgress()
    {
        return 0; //throw new NotImplementedException();
    }

    public float Get2StarProgress()
    {
        return 0; // throw new NotImplementedException();
    }

    public float Get3StarProgress()
    {
        return 0; // throw new NotImplementedException();
    }
}
