﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelMapItem : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI levelTMPTxt;

    [SerializeField] GameObject lockStateVisual;
    [SerializeField] GameObject unlockStateVisual;

    [SerializeField] Button chooseLevelBtn;

    int level;

    public Action LockAction;
    public Action UnlockAction;
    public Action NewLevelAction;

    public Action ChooseButtonPressedAction;

    private void Start()
    {
        chooseLevelBtn.onClick.AddListener(ChooseLvlBtnClickHandler);
    }

    void ChooseLvlBtnClickHandler()
    {
        ChooseButtonPressedAction?.Invoke();
    }


    public void SetLockedState()
    {
        LockAction?.Invoke();
        chooseLevelBtn.enabled = false;
        lockStateVisual.SetActive(true);
        unlockStateVisual.SetActive(false);
        
    }

    public void SetUnlockedState()
    {
        UnlockAction?.Invoke();
        chooseLevelBtn.enabled = true;
        lockStateVisual.SetActive(false);
        unlockStateVisual.SetActive(true);
    }

    public void SetNewLevelState()
    {
        NewLevelAction?.Invoke();
        chooseLevelBtn.enabled = true;
        lockStateVisual.SetActive(false);
        unlockStateVisual.SetActive(true);
        //animator.SetBool("NextLevelIsOn", true);
    }

    public void SetLevel(int _level)
    {
        level = _level;
        levelTMPTxt.text = _level.ToString();
    }

    public int GetLevel()
    {
        return level;
    }

    void OnDestroy()
    {
        chooseLevelBtn.onClick.RemoveListener(ChooseLvlBtnClickHandler);
    }
}
