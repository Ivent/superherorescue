﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;

[RequireComponent(typeof(LevelMapItem))]
public class LevelMapItemLevelLoader : MonoBehaviour
{
    LevelMapItem levelMapItem;
    [Inject] LevelMapView levelMapView;
    [Inject] MenuView menuView;
    [Inject] GameLevelController gameLevelController;

    void Start()
    {
        this.Inject();
        levelMapItem = GetComponent<LevelMapItem>();

        levelMapItem.ChooseButtonPressedAction += LevelMapItem_ChooseButtonPressedAction;
    }

    void LevelMapItem_ChooseButtonPressedAction()
    {
        gameLevelController.SetCurrentLevel(levelMapItem.GetLevel());
        gameLevelController.LoadNextLevel();
        levelMapView.Hide();
        menuView.Close();
    }

    private void OnDestroy()
    {
        levelMapItem.ChooseButtonPressedAction -= LevelMapItem_ChooseButtonPressedAction;
    }
}
