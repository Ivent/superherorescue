﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using UnityEngine.UI;

public class EpisodeController : MonoBehaviour
{
    [Inject] GameLevelController gameLevelController;
    [Inject] TotalStarsCounter totalStarsCounter;

    [SerializeField] int minLevel;
    [SerializeField] int maxLevel;
    [SerializeField] int reqStars;

    [SerializeField] GameObject lockedState;
    [SerializeField] GameObject unlockedState;

    [SerializeField] LevelsField levelsField;

    [SerializeField] Text reqStarTxt;

    bool isInit;

    void Start()
    {
        this.Inject();

        if(isInit == false )
        {
            Init();
            isInit = true;
        }

    }

    private void OnEnable()
    {
        if(isInit)
        Init();
    }

   void Init()
   {
        if (reqStars <= totalStarsCounter.TotalStars)
        {
            unlockedState.SetActive(true);
            lockedState.SetActive(false);
        }
        else
        {
            unlockedState.SetActive(false);
            lockedState.SetActive(true);

            reqStarTxt.text = reqStars.ToString();
        }

        int maxCompLevel = 0;
        if (gameLevelController.MaxLevelIsCompleted())
        {
            maxCompLevel = 1;
        }

        levelsField.InitLevelMap(minLevel, maxLevel, gameLevelController.MaxCompletedLevel, maxCompLevel);

    }
}
