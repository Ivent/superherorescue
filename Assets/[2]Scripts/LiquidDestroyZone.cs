﻿using System.Collections;
using System.Collections.Generic;
using Framework.Common.Camera;
using UnityEngine;

public class LiquidDestroyZone : MonoBehaviour
{
    [SerializeField] GameObject leakPrefab;

    int indicatorPeriod;
    [SerializeField] int defIndicatorPeriod = 10;

    Camera2DFollow c2dFollow;


    private void Start()
    {
        indicatorPeriod = defIndicatorPeriod;
        PoolManager.instance.CreatePool("leakindipool", leakPrefab, 5);
        c2dFollow = FindObjectOfType<Camera2DFollow>();
    }

    float ShowInsideX(float currentX)
    {
        currentX = Mathf.Max(currentX, c2dFollow.BoarderMin.x / 1.4f);
        currentX = Mathf.Min(currentX, (c2dFollow.BoarderMax.x / 1.4f)*1.76f);
        return currentX;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Metaball_liquid")
        {
            indicatorPeriod--;
            collision.gameObject.SetActive(false);
            if (indicatorPeriod == 0)
            {
                Vector3 indiPos = new Vector3(ShowInsideX(collision.transform.position.x), collision.transform.position.y, -62);


                PoolManager.instance.GetObject("leakindipool", indiPos, Quaternion.identity);
                indicatorPeriod = defIndicatorPeriod;
            }
        }
    }
}
