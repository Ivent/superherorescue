﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using Framework.Interfaces;
using UnityEngine;

public class WaterDistanseCounter : MonoBehaviour,ITickSec
{
    [Inject] GameLevelController gameLevelController;
    [Inject] UpdateManager updateManager;

    [SerializeField]
    MetaballParticleClass[] liquidObjects;

    [SerializeField]
    float minDitanse = 999;

    Transform finalPoint;

    Vector3 nearestLiquidObject;

    bool minDistanseFounded;

    public Vector3 NearestLiquidObjectPoint { get => nearestLiquidObject; set => nearestLiquidObject = value; }

    void Start()
    {
        this.Inject();

        gameLevelController.OnLoadStarted += GameLevelController_OnLoadStarted;
        gameLevelController.OnLoadFinished += GameLevelController_OnLoadFinished;

        updateManager.AddTo(this);
    }

    void GameLevelController_OnLoadStarted()
    {
        liquidObjects = null;
    }


    void GameLevelController_OnLoadFinished()
    {
        finalPoint = GameObject.FindWithTag("EndFunnel").transform;

        Invoke("FindBalls", 0.5f);
    }

    void FindBalls()
    {
        liquidObjects = null;
        liquidObjects = Resources.FindObjectsOfTypeAll<MetaballParticleClass>();

        FoundDistance();
    }



    public void TickSec()
    {
        //FoundDistance();
    }

    void FoundDistance()
    {
        float distanse;


        if (liquidObjects != null)
        {

            for (int i = 0; i < liquidObjects.Length; i++)
            {
                if (liquidObjects[i].Active == true && liquidObjects[i] != null)
                {
                    distanse = Vector3.Distance(finalPoint.position, liquidObjects[i].transform.position);
                    if (distanse <= minDitanse)
                    {
                        if (minDitanse - distanse > 1.5f)
                        {
                            minDitanse = distanse;
                            NearestLiquidObjectPoint = liquidObjects[i].transform.localPosition;

                        }
                        minDistanseFounded = true;
                    }
                }
            }
            if (minDistanseFounded == false)
            {
                minDitanse = 999;
            }
            minDistanseFounded = false;
        }
    }

    private void OnDestroy()
    {
        updateManager.RemoveFrom(this);
        gameLevelController.OnLoadStarted -= GameLevelController_OnLoadStarted;
        gameLevelController.OnLoadFinished -= GameLevelController_OnLoadFinished;
    }
}
