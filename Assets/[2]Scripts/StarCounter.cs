﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;

public class StarCounter : MonoBehaviour, IStarCounter
{
    [Range(0,1)]
    [SerializeField] float oneStarLimit;

    [Range(0, 1)]
    [SerializeField] float twoStarLimit;

    [Range(0, 1)]
    [SerializeField] float threeStarLimit;

    [Inject] OilCounter oilCounter;

    [Inject] GameLevelController gameLevelController;

    float prevFillProgress = 1;

    Action<int> onStarChanged;
    Action<int> onStarAdded;

    public Action<int> OnStarChanged { get { return onStarChanged; } set { onStarChanged = value; } }
    public Action<int> OnStarAdded { get { return onStarAdded; } set { onStarAdded = value; } }

    int stars;

    int currentLevel;

    [SerializeField]
    int currentStars;

    bool stop;

    public int CurrentStars { get => currentStars; set => currentStars = value; }
    public int Stars { get => stars; set => stars = value; }

    float star1Progress;
    float star2Progress;
    float star3Progress;

    float star1Score;
    float star2Score;
    float star3Score;

    void Start()
    {
        this.Inject();

        gameLevelController.OnLoadFinished += GameLevelController_OnLoadFinished;
        gameLevelController.OnLoadStarted += GameLevelController_OnLoadStarted;
        oilCounter.OnOilCountChanged += OilCounter_OnOilCountChanged;
    }

    void GameLevelController_OnLoadStarted()
    {
        currentStars = 0;
        star1Score = 0;
        star2Score = 0;
        star3Score = 0;
        star1Progress = 0;
        star2Progress = 0;
        star3Progress = 0;
        prevFillProgress = 1;
        stop = true;
    }


    public float Get1StarProgress()
    {
        return star1Progress;
    }

    public float Get2StarProgress()
    {
        return star2Progress;
    }

    public float Get3StarProgress()
    {
        return star3Progress;
    }

    void OilCounter_OnOilCountChanged()
    {
        if (stop) return;

        float fillProgress = oilCounter.GetFillRatio();

        float deltaFillProgress = Mathf.Abs( prevFillProgress - fillProgress );

        if (currentStars == 0)
        {
            star1Score += deltaFillProgress;
            star1Progress = star1Score / oneStarLimit;
        }

        if (currentStars == 1)
        {
            star2Score += deltaFillProgress;
            star2Progress =  Mathf.Min( star2Score / (twoStarLimit),1);
        }

        if (currentStars == 2)
        {
            star3Score += deltaFillProgress;
            star3Progress = Mathf.Min( star3Score / (threeStarLimit),1);
        }

        prevFillProgress = fillProgress;

        if ( fillProgress <= oneStarLimit)
        {
            if(currentStars == 0)
            {
                currentStars = 1;
                star1Progress = 1;
                OnStarChanged?.Invoke(currentStars);
                SaveStars();
            }
        }

        if (fillProgress <= twoStarLimit)
        {
            if (currentStars == 1)
            {
                currentStars = 2;
                star2Progress = 1;
                OnStarChanged?.Invoke(currentStars);
                SaveStars();
            }               
        }

        if (fillProgress <= threeStarLimit)
        {
            if (currentStars == 2)
            {
                currentStars = 3;
                star3Progress = 1;
                OnStarChanged?.Invoke(currentStars);
                SaveStars();
            }
        }
    }

    void GameLevelController_OnLoadFinished()
    {
        currentStars = 0;
        star1Score = 0;
        star2Score = 0;
        star3Score = 0;
        star1Progress = 0;
        star2Progress = 0;
        star3Progress = 0;

        stop = false;

        currentLevel = gameLevelController.CurrentLevel;

        LoadStars(); 
    }

    void LoadStars()
    {
        Stars = PlayerPrefs.GetInt("s" + currentLevel.ToString());
    }

    void SaveStars()
    {
        if(CurrentStars > Stars)
        {
            int newStars = (currentStars - Stars);
            onStarAdded?.Invoke(newStars);

            Stars += newStars;
  

       
            PlayerPrefs.SetInt("s" + currentLevel.ToString(), Stars);
        }
    }

    private void OnDestroy()
    {
        gameLevelController.OnLoadStarted -= GameLevelController_OnLoadStarted;
        gameLevelController.OnLoadFinished -= GameLevelController_OnLoadFinished;
        oilCounter.OnOilCountChanged -= OilCounter_OnOilCountChanged;
    }

}
