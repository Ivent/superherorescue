﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using UnityEngine.UI;

public class MenuView : MonoBehaviour
{
    bool menuIsOpened = true;

    [SerializeField]
    GameObject visual;

    [SerializeField]
    Button playBtn;

    [SerializeField]
    Button levelsBtn;

    [SerializeField]
    Button settingsBtn;

    [Inject] SettingsWindow settingsWindow;
    [Inject] GameLevelController gameLevelController;
    [Inject] LevelMapView levelMapView;

    int levelOnLoading;

    void Start()
    {

        this.Inject();

        if(menuIsOpened == true)
        {
            Show();
        }

        levelMapView.OnLevelMapBackBtnClicked += LevelMapView_OnLevelMapBackBtnClicked;

        playBtn.onClick.AddListener(PlayBtnClickHandler);
        levelsBtn.onClick.AddListener(LevelsBtnClickHandler);
        settingsBtn.onClick.AddListener(SettingsBtnClickHandler);

        gameLevelController.OnLoadFinished += GameLevelController_OnLoadFinished;


    }

    void LevelMapView_OnLevelMapBackBtnClicked()
    {
        Show();
    }


    void GameLevelController_OnLoadFinished()
    {
        levelOnLoading = gameLevelController.CurrentLevel;
    }


    void PlayBtnClickHandler()
    {
       // if (levelOnLoading == gameLevelController.CurrentLevel)
       // {
            Close();
       // }
       // else
       // {
            gameLevelController.LoadNextLevel();
           // Close();
       // }
    }


    void LevelsBtnClickHandler()
    {
        levelMapView.Show();
    }


    void SettingsBtnClickHandler()
    {
        settingsWindow.Show();
    }

    public void Show()
    {
        menuIsOpened = true;
        visual.SetActive(true);
    }

    public void Close()
    {
        menuIsOpened = false;
        visual.SetActive(false);
    }

    private void OnDestroy()
    {
        levelMapView.OnLevelMapBackBtnClicked -= LevelMapView_OnLevelMapBackBtnClicked;

        playBtn.onClick.RemoveListener(PlayBtnClickHandler);
        levelsBtn.onClick.RemoveListener(LevelsBtnClickHandler);
        settingsBtn.onClick.RemoveListener(SettingsBtnClickHandler);

        gameLevelController.OnLoadFinished -= GameLevelController_OnLoadFinished;
    }
}
