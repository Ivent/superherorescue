﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using DG.Tweening;
using UnityEngine;

public class Car : MonoBehaviour
{

    [Inject] GameWinController gameWinController;
    [Inject] OilCounter oilCounter;

    [SerializeField] Vector3 moveOffset = new Vector3(-15, 0, 0);

     Vector3 finishPoint;
    [SerializeField] float timeToFinish;

    [SerializeField] Transform progressLine;

    [SerializeField]
    Animator animator;

    void Start()
    {
        this.Inject();
        finishPoint = transform.position + moveOffset;

        oilCounter.OnOilCountChanged += OilCounter_OnOilCountChanged;

        gameWinController.OnGameWin += GameWinController_OnGameWin;
    }

    void OilCounter_OnOilCountChanged()
    {
        progressLine.localScale = new Vector3(progressLine.localScale.x, 1-oilCounter.GetFillRatio(), progressLine.localScale.z);
    }


    void GameWinController_OnGameWin()
    {
        animator.SetTrigger("moveTrigger");
        transform.DOMove(finishPoint, timeToFinish);
    }

    private void OnDestroy()
    {
        oilCounter.OnOilCountChanged -= OilCounter_OnOilCountChanged;
        gameWinController.OnGameWin -= GameWinController_OnGameWin;
    }
}
