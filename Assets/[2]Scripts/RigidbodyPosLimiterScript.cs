﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigidbodyPosLimiterScript : MonoBehaviour
{
    Rigidbody2D rigidbody2d;

    bool isActive = true;

    void Awake()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            if (transform.position.y > 120)
            {
                rigidbody2d.Sleep();
                isActive = false;
                Destroy(gameObject);
            }
        }
    }
}
