﻿using System.Collections;
using System.Collections.Generic;
using Framework.Common.Camera;
using UnityEngine;

public class CameraLiquidFollow : MonoBehaviour
{
    WaterDistanseCounter waterDistanseCounter;
    Camera2DFollow camera2DFollow;
    CameraGuide cameraGuide;

    float followSpeed;

    bool isWork;


    void Start()
    {
        waterDistanseCounter = FindObjectOfType<WaterDistanseCounter>();
        camera2DFollow = GetComponent<Camera2DFollow>();
        cameraGuide = FindObjectOfType<CameraGuide>();
        isWork = false;

        Invoke("Test", 1);
    }

    void Test()
    {
        isWork = true;
        cameraGuide.ChangeY(waterDistanseCounter.NearestLiquidObjectPoint.y);
    }

    private void Update()
    {
        if (!isWork) return;

        Vector3 newPos = new Vector3(transform.position.x, cameraGuide.transform.position.y-10, transform.position.z);
        camera2DFollow.SetTargetPos(newPos);
    }
}
