﻿using Adic;
using Framework;
using Framework.Common;
using Framework.Managers.Ads;
using Framework.Managers.Statistics;

public class GameSceneContext : BaseMainContext
{
    public GameLevelController gameLevelController;
    public SettingsWindow settingsWindowView;
    public MenuView menuView;
    public LevelMapView levelMapView;
    public AudioSystem audioSystem;

    protected override void BindCommands()
    {
       
    }

    protected override void BindComponents()
    {
       // this.containers[0]
         //.Bind<BuildingBlockCounter>().ToSingleton();
    }
    protected override void BindConfigs() { }

    protected override void BindControllers()
    {
        //this.containers[0]
           //.Bind<SoundSwitcherController>().ToSingleton();
    }

    protected override void BindManagers() {
        this.containers[0]
             .Bind<UpdateManager>().ToGameObject()
             .Bind<StatisticSystemController>().ToSingleton()
             .Bind<BaseAdSystem>().To<DummyAdSystem>();
    }

    protected override void BindModels()
    {
       // this.containers[0]
            //.Bind<SwitchModel>().ToSingleton<SwitchSoundModel>().As("soundSwitcherModel");
    }

    protected override void BindView()
    {
        this.containers[0]
             .Bind<IAudioSystem>().To(audioSystem)
             .Bind<LevelPrefabLoader>().ToGameObject()
             .Bind<GameLevelController>().To(gameLevelController)
             .Bind<TargetsOnLevel>().ToGameObject()
             .Bind<SettingsWindow>().To(settingsWindowView)
             .Bind<GameWinController>().ToGameObject()
             .Bind<GameLooseController>().ToGameObject()
             .Bind<GameTargetUpdateAvaibleStateEvent>().ToSingleton()
             .Bind<LevelMapView>().To(levelMapView)
             .Bind<MenuView>().To(menuView);








    }
}
