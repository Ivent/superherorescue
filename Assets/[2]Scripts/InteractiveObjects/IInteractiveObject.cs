﻿public interface IInteractiveObject
{
    void Interact();
}