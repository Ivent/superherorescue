﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveButton : MonoBehaviour
{
    [SerializeField] GameObject ineractableObject;

    [SerializeField] bool isUsed;

    public Action OnInteract;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isUsed == true) return;

        if (collision.tag == "Metaball_liquid")
        {
            ineractableObject.GetComponent<IInteractiveObject>().Interact();
            isUsed = true;
            OnInteract?.Invoke();
        }
        
    }
}
