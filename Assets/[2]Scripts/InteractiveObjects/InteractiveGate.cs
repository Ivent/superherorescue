﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class InteractiveGate : MonoBehaviour, IInteractiveObject
{
    [SerializeField] Transform gate;

    [SerializeField] Transform openGatePos;
    [SerializeField] Transform closedGateTransform;

    [SerializeField] Transform gateVisual;

    [SerializeField] float openSpeed;
    [SerializeField] float closeSpeed;

    bool isOpened = true;

    bool isWorking;

    private void Start()
    {
        if (isOpened)
        {
            gate.position = openGatePos.position;
        }
        else
        {
            gate.position = closedGateTransform.position;
        }
    }

    public void Interact()
    {
        SwitchState();
    }

    void SwitchState()
    {
        if (isWorking) return;

        if(isOpened)
        {
            CloseGate();
        }
        else
        {
            OpenGate();
        }

        isWorking = true;
        isOpened = !isOpened;

    }

    void OpenGate()
    {
        gate.DOMove(openGatePos.localPosition, openSpeed).OnComplete(() =>
        {
            isWorking = false;
        });

        gateVisual.DOScaleX(6, openSpeed);
    }

    void CloseGate()
    {
        gate.DOMove(closedGateTransform.position, closeSpeed).OnComplete(() =>
        {
            isWorking = false;
        });

        gateVisual.DOScaleX(0, closeSpeed);
    }
}
