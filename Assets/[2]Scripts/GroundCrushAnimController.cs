﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Adic;
using Framework.Interfaces;
using UnityEngine;

using Vector2f = UnityEngine.Vector2;


public class GroundCrushAnimController : MonoBehaviour
{
    Vector2 mousePos;
    Vector2 lastMousePos;

    [Inject] GameLevelController gameLevelController;

    [SerializeField] LayerMask collisionLayerMask;

    [SerializeField] Transform groundEmitterPos;

    [SerializeField] ParticleSystem groundEmitterPosParticleSys;

     DestructibleTerrain terrain;

    Camera mainCamera;

    private Vector2f currentTouchPoint;

    private Vector2f previousTouchPoint;

    private void Awake()
    {
       
    }

    private void Start()
    {
        this.Inject();

        gameLevelController.OnLoadFinished += GameLevelController_OnLoadFinished;
    }

    void GameLevelController_OnLoadFinished()
    {
        terrain = FindObjectOfType<DestructibleTerrain>();
        mainCamera = Camera.main;

    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (terrain == null) return;
            if (HelpFunc.IsPointerOverGameObject() == true || Camera.main == null) return;
        }

            if (Input.GetMouseButton(0))
        {

            Vector2 XOYPlaneLocation = mainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));
            currentTouchPoint = XOYPlaneLocation;// - terrain.GetPositionOffset();

            groundEmitterPos.position = new Vector3( currentTouchPoint.x, currentTouchPoint.y, groundEmitterPos.position.z);

            if (previousTouchPoint != currentTouchPoint)
            {
                groundEmitterPosParticleSys.Emit(1);
            }

            previousTouchPoint = currentTouchPoint;

            //if ( HelpFunc.IsPointerOverGameObject() == true || Camera.main == null)
            //{
            //    return;
            //}

            //mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            //var origin = new Vector2(mousePos.x, mousePos.y);
            //RaycastHit2D[] hits = Physics2D.RaycastAll(origin, Vector2.zero, 0f, collisionLayerMask).OrderBy(h => h.distance).ToArray();

            //if (hits.Length > 0 && hits[hits.Length - 1].collider != null)
            //{



            //}

        }

        if (Input.GetMouseButtonUp(0))
        {
            groundEmitterPosParticleSys.Stop();
        }
    }

    private void OnDestroy()
    {
        gameLevelController.OnLoadFinished -= GameLevelController_OnLoadFinished;
    }
}
