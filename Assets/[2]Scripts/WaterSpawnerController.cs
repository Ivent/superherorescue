﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using Water2D;

public class WaterSpawnerController : MonoBehaviour
{
    [Inject] GameLevelController gameLevelController;

    [SerializeField] Water2D_Spawner waterSpawner;

    void Start()
    {
        this.Inject();
        gameLevelController.OnLoadStarted += GameLevelController_OnLoadStarted;
    }

    void GameLevelController_OnLoadStarted()
    {
        waterSpawner.Restore();
    }

    private void OnDestroy()
    {
        gameLevelController.OnLoadStarted -= GameLevelController_OnLoadStarted;
    }
}
