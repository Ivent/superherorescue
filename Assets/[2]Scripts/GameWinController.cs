﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;

public class GameWinController : MonoBehaviour
{
    [Inject] GameLevelController gameLevelController;
 
    public Action OnGameWin = delegate {};

    bool onGameWin;

    void Start()
    {
        this.Inject();

        gameLevelController .OnLoadStarted += GameLevelController_OnLoadStarted;

    

    }

    void GameLevelController_OnLoadStarted()
    {
        onGameWin = false;
    }



    public void GameWin()
    {
        if (onGameWin) return;

        onGameWin = true;

        OnGameWin.Invoke();
        gameLevelController.SetCurrentLevel(gameLevelController.CurrentLevel + 1);
        gameLevelController.Restart();
    }

    private void OnDestroy()
    {
        gameLevelController.OnLoadStarted -= GameLevelController_OnLoadStarted;
    }
}
