﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;

public class GameLooseController : MonoBehaviour
{
    ExplosiveActivator[] explosiveActivators;

    [Inject] GameLevelController gameLevelController;

    public Action OnLoose;

    bool eventsListiening;

    void Start()
    {
        gameLevelController.OnLoadFinished += GameLevelController_OnLoadFinished;

     
    }

    void GameLevelController_OnLoadFinished()
    {
        UnSubscr();
        SubscToEvents();
    }

    void SubscToEvents()
    {
        if (eventsListiening == true) return;

        eventsListiening = true;
        explosiveActivators = FindObjectsOfType<ExplosiveActivator>();
        for (int i = 0; i < explosiveActivators.Length; i++)
        {
            explosiveActivators[i].OnBoom += Handle_OnBoom;
        }
    }

    void UnSubscr()
    {
        if (eventsListiening == false) return;

         eventsListiening = false;
        for (int i = 0; i < explosiveActivators.Length; i++)
        {
            explosiveActivators[i].OnBoom -= Handle_OnBoom;
        }
    }

    void Restart()
    {
        gameLevelController.SetCurrentLevel(gameLevelController.CurrentLevel);
        gameLevelController.Restart();
    }


    void Handle_OnBoom()
    {
        Restart();
        OnLoose?.Invoke();
  
    }

    private void OnDestroy()
    {
        gameLevelController.OnLoadFinished -= GameLevelController_OnLoadFinished;
       
    }
}
