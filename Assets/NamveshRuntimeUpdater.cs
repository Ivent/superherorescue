﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NamveshRuntimeUpdater : MonoBehaviour
{
    [SerializeField] NavMeshSurface NavMeshSurface;

    [SerializeField] float defNavmeshRefreshRate = 0.25f;

    [SerializeField] float navmeshRefreshRate; 

    void Start()
    {
        defNavmeshRefreshRate = navmeshRefreshRate;
    }

    // Update is called once per frame
    void Update()
    {
        navmeshRefreshRate -= Time.deltaTime;
        if(defNavmeshRefreshRate <= 0)
        {
            navmeshRefreshRate = defNavmeshRefreshRate;
            NavMeshSurface.BuildNavMesh();
        }

    }
}
